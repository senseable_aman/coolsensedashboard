
import 'package:coolsense_manager_app/routes.dart';
import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/views/pages/cover/cover_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Coolsense',
      theme: new ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: AppStyles.blueColor
      ),
      home: new CoverPage(),
      routes: routes,
    );
  }
}

