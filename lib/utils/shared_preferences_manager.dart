
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesManager {

  final String _keyToken = "token";
  final String _keyLocationId = "locationId";
  final String _keyEntityId = "entityId";
  final String _keyEntityName = "entityName";
  final String _isLogin = "isLogin";
  final String _keyUserName = "userName";
  final String _keyPassword = "password";
  final String _keyUserEmail = "useremail";

/*
  Future<bool> getAllowsNotifications() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getBool(_keyAllowNotifications) ?? false;
  }

  Future<bool> setAllowsNotifications(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setBool(_keyAllowNotifications, value);
  }*/

  Future<bool> getIsLogin() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(_isLogin) ?? false;
  }
  Future<bool> setIsLogin(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(_isLogin, value);
  }

  Future<String> getToken() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_keyToken) ?? '';
  }

  Future<bool> setToken(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_keyToken, value);
  }

  Future<String> getLocationId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_keyLocationId) ?? '';
  }

  Future<bool> setLocationId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_keyLocationId, value);
  }

  Future<String> getEntityId() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_keyEntityId) ?? '';
  }

  Future<bool> setEntityId(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_keyEntityId, value);
  }

  Future<String> getEntityName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_keyEntityName) ?? '';
  }

  Future<bool> setEntityName(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_keyEntityName, value);
  }

  Future<String> getUserName() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_keyUserName) ?? '';
  }

  Future<bool> setUserName(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_keyUserName, value);
  }

  Future<String> getPassword() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_keyPassword) ?? '';
  }

  Future<bool> setPassword(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_keyPassword, value);
  }

  Future<String> getUserEmail() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.getString(_keyUserEmail) ?? '';
  }

  Future<bool> setUserEmail(String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.setString(_keyUserEmail, value);
  }

  Future clearAllPref() async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    preferences.clear();
  }

}

