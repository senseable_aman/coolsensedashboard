import 'package:flutter/material.dart';

class AppStyles {
  static final Color blueColor = new Color(0xFF15587C);
  static final Color orangeColor = new Color(0xFFFF7518 );
  static final Color grayColor = new Color(0xFFDDDDDD);
  static final Color lightGrayColor = new Color(0xFFEEEEEE);
  static final Color greenColor = new Color(0xFF08AE55);
  static final Color lightGreenColor = new Color(0xFF97C855);
  static final Color pinkColor = new Color(0xFFE5B7B6);
  static final Color yellowColor = new Color(0xFFFCC108);
  static final Color redColor = new Color(0xFFEC2124);
  static final Color darkRedColor = new Color(0xFFBE2124);
  static final Color lightBlueColor = new Color(0xFF20a8d8);

}
