import 'dart:async';

import 'package:coolsense_manager_app/loader/color_loader.dart';
import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/view_models/alerts_fragment_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class AlertsFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new AlertsFragmentState();
}

class AlertsFragmentState extends State<AlertsFragment> {
  AlertsFragmentViewModel _alertsViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _alertsViewModel = new AlertsFragmentViewModel();
    _alertsViewModel.getToken();
    new Timer(const Duration(milliseconds: 400), () {
      setState(() {
        _alertsViewModel.getAlertsData();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ScopedModel<AlertsFragmentViewModel>(
        model: _alertsViewModel,
        child: new Scaffold(
          appBar: appBar(),
          body: body(),
        ));
  }

  Widget appBar() {
    return new AppBar(
        backgroundColor: AppStyles.blueColor,
        title: new ScopedModelDescendant<AlertsFragmentViewModel>(
            builder: (context, child, model) {
              return new Row(
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.only(right: 7.0)),
                  new Flexible(
                      child: new Text("Alerts",
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontFamily : 'Roboto'
                        ),
                        overflow: TextOverflow.ellipsis,

                      )
                  )

                ],
              );
            }
        )
    );
  }


  Widget body() {
    return new ScopedModelDescendant<AlertsFragmentViewModel>(builder: (context ,child, model) {
      return new Container(
          width: double.infinity,
          padding: new EdgeInsets.all(10.0),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              model.isLoading? loader(): model.alertsList.isEmpty ? noAlertData() : new Container(
                  child: new Expanded(
                    child:
                      alertsData(),
                  )
              )
            ],
          )
      );
    });
  }

  Widget alertsData(){
    return new ScopedModelDescendant<AlertsFragmentViewModel>(builder: (context, child, model) {
      return Container(
        child: new ListView.builder(
            shrinkWrap: true,
            itemCount: model.alertsList == null ? 0 : model.alertsList.length,
            itemBuilder: (context, index) {
              return new Card(
                child: new Container(
                  padding: new EdgeInsets.all(8.0),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(model.alertsList[index].entityName == null
                          ? ""
                          : model.alertsList[index].entityName,
                        style: TextStyle(fontWeight: FontWeight.bold,
                            fontSize: 12.0,
                            fontFamily: 'Roboto'),
                      ),
                      new Padding(padding: EdgeInsets.all(5.0)),
                      new Row(
                        children: <Widget>[
                          new Text("Start Time :",
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                          new Padding(padding: EdgeInsets.only(right: 5.0)),
                          new Text(model.alertsList[index].alertOpenedTime,
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                      new Padding(padding: EdgeInsets.all(3.0)),
                      new Row(
                        children: <Widget>[
                          new Text("Duration :",
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                          new Padding(padding: EdgeInsets.only(right: 5.0)),
                          new Text((DateTime.parse(model.alertsList[index].alertCloseTime).difference(DateTime.parse(model.alertsList[index].alertOpenedTime))).inMinutes.toString() + ' min',
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                      new Padding(padding: EdgeInsets.all(3.0)),
                      new Row(
                        children: <Widget>[
                         /* new Text("High Temp :",
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                          new Padding(padding: EdgeInsets.only(right: 5.0)),*/
                          new Text(model.alertsList[index].alertType =='MAX_TEMP' ? 'High Temp :'+model.alertsList[index].alertDesc.split('=')[2].split('%')[0]+' %'
                              : model.alertsList[index].alertType =='MIN_TEMP' ? 'Low Temp :'+model.alertsList[index].alertDesc.split('=')[2].split('%')[0]+' %'
                              : model.alertsList[index].alertType =='MAX_RH' ? 'High Humidity :'+model.alertsList[index].alertDesc.split('=')[2].split('%')[0]+' %'
                              :'Low Humidity :'+model.alertsList[index].alertDesc.split('=')[2].split('%')[0]+' %' ,
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                      new Padding(padding: EdgeInsets.all(3.0)),
                      new Row(
                        children: <Widget>[
                          new Text("Threshold :",
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                          new Padding(padding: EdgeInsets.only(right: 5.0)),
                          new Text(model.alertsList[index].alertDesc.split('=')[1].split('%')[0]+' %',
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              );
            }
        ),
      );
    });
  }


  Widget noAlertData(){
    return new ScopedModelDescendant<AlertsFragmentViewModel>(builder: (context ,child, model){
      return   new Container(
        width: double.infinity,
        padding: new EdgeInsets.all(10.0),
        child: new Column(

          children: <Widget>[
            new Image.asset('images/ic_no_alert.png',
              height: 80,
            ),
            new Padding(padding: EdgeInsets.only(top: 10.0)),
            new Container(
              margin: const EdgeInsets.all(15.0),
              padding: const EdgeInsets.all(3.0),
              child :new Center(
                child: Text("No Alerts found",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0, color: AppStyles.blueColor,  fontFamily : 'Eurostib'),
                ),
              ),
            ),
          ],
        ),
      );
    });

  }

  Widget loader(){
    return new Container(
      padding: EdgeInsets.all(8.0),
      child: new ColorLoader(),
    );

  }
}