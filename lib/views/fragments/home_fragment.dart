import 'dart:async';

import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/view_models/home_fragment_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class HomeFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new HomeFragmentState();
}

class HomeFragmentState extends State<HomeFragment> {
  HomeFragmentViewModel _model;
  TextEditingController _controller;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _model = new HomeFragmentViewModel();
    _controller = new TextEditingController();
    _model.getToken();
     new Timer(const Duration(milliseconds: 400), () {
      setState(() {
        _model.getDeviceData();

      });
    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ScopedModel<HomeFragmentViewModel>(
        model: _model,
        child: Scaffold(
          appBar: appBar(),
          body: body(),
        )
    );
  }


  Widget appBar() {
    return new AppBar(

      backgroundColor: AppStyles.blueColor,
      title: new Row(
        children: <Widget>[
          new Padding(padding: new EdgeInsets.only(left: 7.0)),
          new Flexible(
              child: new Text("Dashboard",
                style: new TextStyle(
                  color: Colors.white,
                  fontSize: 18.0,
                  fontFamily: 'Roboto',
                ),
                overflow: TextOverflow.ellipsis,

              )
          )
        ],
      ),
    );
  }

  Widget body() {
    _model.buildContext(context);
    return new ScopedModelDescendant<HomeFragmentViewModel>(
        builder: (context, child, model) {
          return new Container(
              width: double.infinity,
              child: new Column(
                children: <Widget>[
                  searchBox(),
                  new Flexible(
                      child:   model.isLoading ? loader() : deviceGrid()
                  )
                ],
              )
          );
        });
  }
  Widget deviceGrid() {
    return new ScopedModelDescendant<HomeFragmentViewModel>(
        builder: (context, child, model) {
          var list = model.filterDeviceList;
          return new GridView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.all(10.0),
              gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                crossAxisSpacing: 8.0,
                mainAxisSpacing: 5.0,
              ),
              itemCount: list == null ? 0 : list.length,
              itemBuilder: (context, index) {
                return new GestureDetector(
                    onTap: () {
                      model.clickRoomWiseData(context, index);
                    },
                    child: new GridTile(
                        child: new Column(
                          children: <Widget>[
                            new Card(
                              child: Center(
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: new Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                       list[index].temp.id != 0 ? deviceTempData(index) : list[index].gpsCompVo.id!=0 ? deviceGPSData(index) :deviceGasMeterData(index),

                                    ],
                                  ),
                                ),
                              )
                              ,
                            )
                          ],
                        )
                    )
                );
              }
          );
        }
    );
  }
  Widget loader(){
    return new Container(
      padding: EdgeInsets.all(8.0),
      child: new CircularProgressIndicator(),
    );
  }


  Widget searchBox() {
    return new ScopedModelDescendant<HomeFragmentViewModel>(
        builder: (context, child, model) {
          return Container(
              height: 40.0,
              margin: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: Colors.black12,
                borderRadius: BorderRadius.all(Radius.circular(32)),
              ),
              child:
              new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      textAlign: TextAlign.left,
                      controller: _controller,
                      keyboardType: TextInputType.text,
                      onChanged: (val) => model.filterSearchResults(val),
                      decoration: new InputDecoration(
                        hintStyle: TextStyle(fontSize: 12),
                        suffixIcon: Icon(Icons.search),
                        border: InputBorder.none,
                        contentPadding: EdgeInsets.only(left: 20.0, top: 10.0),
                        hintText: "Search Device Name and location",
                      ),
                    ),
                  )
                ],
              )
          );
        }
    );
  }


  Widget deviceTempData(int index) {
    return new ScopedModelDescendant<HomeFragmentViewModel>(
        builder: (context, child, model) {
          return new Container(
              child : new Column(
                children: <Widget>[
                  new Center(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(model.filterDeviceList[index].entityName == null
                              ? 0
                              : model.filterDeviceList[index].entityName,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 11.0,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                          new Padding(padding: EdgeInsets.all(10.0)),
                          new Text(
                            model.filterDeviceList[index].temp.value.toString() +
                                "°C" == null ? 0 : model.filterDeviceList[index]
                                .temp.value.toString() + "°C",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22.0,
                                color: Colors.lightGreen,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )
          );
        }
    );
  }

  Widget deviceGasMeterData(int index) {
    return new ScopedModelDescendant<HomeFragmentViewModel>(
        builder: (context, child, model) {
          return new Container(
              child : new Column(
                children: <Widget>[
                  new Center(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(model.filterDeviceList[index].entityName == null
                              ? 0
                              : model.filterDeviceList[index].entityName,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15.0,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                          new Padding(padding: EdgeInsets.all(10.0)),
                          new Text(
                            model.filterDeviceList[index].gasMeter.value == 0.0 ? 0 : model.filterDeviceList[index]
                                .gasMeter.value.toString(),
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 22.0,
                                color: Colors.lightGreen,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )
          );
        }
    );
  }


  Widget deviceGPSData(int index) {
    return new ScopedModelDescendant<HomeFragmentViewModel>(
        builder: (context, child, model) {
          return new Container(
              child : new Column(
                children: <Widget>[
                  new Center(
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text(model.filterDeviceList[index].entityName == null
                              ? 0
                              : model.filterDeviceList[index].entityName,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 15.0,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                          new Padding(padding: EdgeInsets.all(10.0)),
                          new Text(model.address,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 10.0,
                                color: Colors.lightGreen,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                          new Text("",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                color: Colors.grey,
                                fontFamily: 'Roboto'),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              )
          );
        }
    );
  }

}



