import 'dart:async';

import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/view_models/profile_fragment_view_model.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ProfileFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new ProfileFragmentState();
}

class ProfileFragmentState extends State<ProfileFragment> {
  ProfileFragmentViewModel _profileViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _profileViewModel = new ProfileFragmentViewModel();
    _profileViewModel.buildContext(context);
    new Timer(const Duration(milliseconds: 400), () {
      setState(() {
        _profileViewModel.getUserName();
        _profileViewModel.getPassword();
        _profileViewModel.getUserEmail();
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ScopedModel<ProfileFragmentViewModel>(
        model: _profileViewModel,
        child: new Scaffold(
          appBar: appBar(),
          body: body(),
        ));
  }

  Widget appBar() {
    return new AppBar(
        backgroundColor: AppStyles.blueColor,
        title: new ScopedModelDescendant<ProfileFragmentViewModel>(
            builder: (context, child, model) {
              return new Row(
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.only(right: 7.0)),
                  new Flexible(
                      child: new Text("Profile",
                        style: new TextStyle(
                          color: Colors.white,
                          fontSize: 18.0,
                          fontFamily : 'Roboto'
                        ),
                        overflow: TextOverflow.ellipsis,

                      )
                  )

                ],
              );

            }
        )
    );
  }


  Widget body() {
    return new ScopedModelDescendant<ProfileFragmentViewModel>(
        builder: (context, child, model) {
          return Container(
              child: new Column(
                children: <Widget>[
                  userLogout(),
                  new Padding(padding: new EdgeInsets.all(10.0)),
                  userChangePassword(),

                ],
              )


          );
        });
  }


  Widget userLogout(){
    return new ScopedModelDescendant<ProfileFragmentViewModel>(
        builder: (context, child, model){
          return new Card(
              margin: EdgeInsets.all(8.0),
              color: Colors.white,
              elevation: 4.0,
              child:new Container(
                child:  new Row(
                  children: <Widget>[
                    new Container(
                      width:200,
                      padding: EdgeInsets.only(left: 12.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Text( model.userName == null ? "" :  model.userName.toString(),
                              style: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12.0,
                                  fontFamily: 'Roboto'
                              )),
                          new Text(model.userEmail == null ? "" : model.userEmail.toString(),
                              style: new TextStyle(
                                  color: Colors.grey,
                                  fontSize: 12.0,
                                  fontFamily: 'Roboto'
                              )),
                        ],
                      ),
                    ),
                    new Container(

                      child: new Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          new RaisedButton(
                            onPressed: () {
                              model.doLogout(model.userEmail, model.userPassword);
                            },
                            textColor: Colors.white,
                            color: AppStyles.blueColor,
                            padding: const EdgeInsets.all(8.0),
                            child: Text('Logout ',
                                style: TextStyle(fontFamily: 'Roboto', color: Colors.white)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ));
        });
  }

  Widget userChangePassword(){
    return new ScopedModelDescendant<ProfileFragmentViewModel>(
        builder: (context, child, model) {
          return new Container(
            child: new Card(
                margin: EdgeInsets.all(8.0),
                color: Colors.white,
                elevation: 4.0,
                child: new Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    new Padding(padding: EdgeInsets.only(left: 10.0, top: 8.0),
                        child: new Text("Change Password",
                            style: new TextStyle(
                                color: AppStyles.blueColor,
                                fontSize: 16.0,
                                fontFamily: 'Roboto'
                            ))),
                    new Padding(padding: new EdgeInsets.all(5.0)),
                    new Row(
                      children: <Widget>[
                        new Padding(padding: EdgeInsets.only(left: 10.0),
                            child: new Text("Username :",
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                    fontFamily: 'Roboto'
                                ))),
                        new Padding(padding: new EdgeInsets.only(left: 10.0)),
                        new Text("user@pharma.com",
                            style: new TextStyle(
                                color: Colors.grey,
                                fontSize: 12.0,
                                fontFamily: 'Roboto'
                            )),
                      ],
                    ),
                    new Padding(padding: new EdgeInsets.all(5.0)),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Padding(padding: EdgeInsets.only(left: 10.0),
                            child: new Text("Old Pass   :",
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                    fontFamily: 'Roboto'
                                ))),
                        new Padding(padding: new EdgeInsets.only(
                            left: 10.0)),
                        new Expanded(child: new TextFormField(
                          decoration: InputDecoration(
                            //Add th Hint text here.
                            hintText: "Old Password",
                            contentPadding: new EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                            border: OutlineInputBorder(borderSide: BorderSide(
                                color: Colors.red,
                                width: 0.0),
                            ),
                          ),
                        ),),
                        new Padding(padding: new EdgeInsets.only(
                            right: 10.0)),
                        //  )
                      ],
                    ),
                    new Padding(padding: new EdgeInsets.all(5.0)),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Padding(padding: EdgeInsets.only(left: 10.0),
                            child: new Text("New Pass :",
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                    fontFamily: 'Roboto'
                                ))),
                        new Padding(padding: new EdgeInsets.only(
                            left: 10.0)),
                        new Expanded(child: new TextFormField(
                          decoration: InputDecoration(
                            //Add th Hint text here.
                            hintText: "New Password",
                            contentPadding: new EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                            border: OutlineInputBorder(borderSide: BorderSide(
                                color: Colors.red,
                                width: 0.0),
                            ),
                          ),
                        ),),
                        new Padding(padding: new EdgeInsets.only(
                            right: 10.0)),
                        //  )
                      ],
                    ),
                    new Padding(padding: new EdgeInsets.all(5.0)),
                    new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        new Padding(padding: EdgeInsets.only(left: 10.0),
                            child: new Text("Confirm     :",
                                style: new TextStyle(
                                    color: Colors.grey,
                                    fontSize: 12.0,
                                    fontFamily: 'Roboto'
                                ))),
                        new Padding(padding: new EdgeInsets.only(
                            left: 10.0)),
                        new Expanded(child: new TextFormField(
                          decoration: InputDecoration(
                            //Add th Hint text here.
                            hintText: "Confirm New Password",
                            contentPadding: new EdgeInsets.symmetric(vertical: 5.0, horizontal: 10.0),
                            border: OutlineInputBorder(borderSide: BorderSide(
                                color: Colors.red,
                                width: 0.0),
                            ),
                          ),
                        ),),
                        new Padding(padding: new EdgeInsets.only(
                            right: 10.0, bottom: 10.0)),

                        //  )
                      ],
                    ),
                    const SizedBox(height: 10),
                    new Padding(padding: EdgeInsets.all(10.0),
                      child: buttonSubmit(),)
                  ],
                )
            ),
          );
        });
  }


  Widget buttonSubmit(){
    return ScopedModelDescendant<ProfileFragmentViewModel>(
      builder: (context, child, model) {
        return new Row(
          mainAxisAlignment: MainAxisAlignment.end,

          children: <Widget>[
            new RaisedButton(
              onPressed: () {

              },
              textColor: Colors.white,
              color: AppStyles.blueColor,
              padding: const EdgeInsets.all(8.0),
              child: Text('Submit',
                  style: TextStyle(fontFamily: 'Roboto', color: Colors.white)),
            ),
          ],
        );
      },
    );
  }
}
