
import 'dart:async';

import 'package:coolsense_manager_app/loader/color_loader.dart';
import 'package:coolsense_manager_app/model/device_data.dart';
import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/view_models/report_fragment_view_model.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class ReportFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new ReportFragmentState();
}

class ReportFragmentState extends State<ReportFragment> {
  ReportFragmentViewModel _reportViewModel;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _reportViewModel = new ReportFragmentViewModel();
    _reportViewModel.getToken();
    new Timer(const Duration(milliseconds: 400), () {
      setState(() {
        _reportViewModel.getDeviceData();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    // TODO: implement build
    return new ScopedModel(
        model: _reportViewModel,
        child: new Scaffold(
          appBar: appBar(),
          body: body(),
        ));
  }

  Widget appBar() {
    return new AppBar(
        backgroundColor: AppStyles.blueColor,
        title: new ScopedModelDescendant<ReportFragmentViewModel>(
            builder: (context, child, model) {
              return new Row(
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.only(right: 7.0)),
                  new Flexible(
                      child: new Text("Reports",
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontFamily : 'Roboto'
                        ),
                        overflow: TextOverflow.ellipsis,

                      )
                  )

                ],
              );

            }
        )
    );
  }


  Widget body() {
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model){
       return reportView();
      },);
  }


  Widget reportView(){
    return new ScopedModelDescendant<ReportFragmentViewModel>(
        builder: (context, child, model) {
          return new Column(
            children: <Widget>[
              new Container(
                child: new Card(
                    margin: EdgeInsets.all(8.0),
                    color: Colors.white,
                    elevation: 4.0,
                    child: new Container(
                      padding: new EdgeInsets.all(10.0),
                      child: new Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          new Padding(
                              padding: const EdgeInsets.only(left: 30)),
                          new Text("Download PDF Reports",
                            textAlign: TextAlign.start,
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 15.0,
                                fontFamily: 'Roboto'),
                          ),

                          dropDownButton(),
                          new Text("Select Duration:",
                            textAlign: TextAlign.left,
                            style: TextStyle(fontWeight: FontWeight.bold,
                                fontSize: 8.0,
                                fontFamily: 'Roboto'),
                          ),
                          //radioButton(),
                          customDatePicker(),
                          viewPDF()

                        ],
                      ),
                    )
                ),
              )
            ],
          );
        });
  }

  Widget radioButton(){
    return new ScopedModelDescendant<ReportFragmentViewModel>(
        builder: (context, child, model) {
          return new Container(
            child: new Column(
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new Radio(
                      value: 1,
                      groupValue: model.selectedRadio,
                      activeColor: AppStyles.blueColor,
                      onChanged: (val){
                        model.setSelectedRadio(val);
                        model.setShowCustom(false);
                      },
                    ),
                    new Text(
                      'Last 24 hours',
                      style: new TextStyle(fontSize: 10.0),
                    ),

                    new Padding(
                        padding: const EdgeInsets.only(left:80.0)),
                    new Radio(
                      value: 2,
                      groupValue: model.selectedRadio,
                      activeColor: AppStyles.blueColor,
                      onChanged: (val){
                        model.setSelectedRadio(val);
                        model.setShowCustom(false);
                      },
                    ),
                    new Text(
                      'Last 7 days',
                      style: new TextStyle(fontSize: 10.0),
                    ),
                  ],
                ),
                new Row(
                  children: <Widget>[
                    new Radio(
                      value: 3,
                      groupValue: model.selectedRadio,
                      activeColor: AppStyles.blueColor,
                      onChanged: (val){
                        model.setSelectedRadio(val);
                        model.setShowCustom(false);
                      },
                    ),
                    new Text(
                      'Last 1 month',
                      style: new TextStyle(fontSize: 10.0),
                    ),

                    new Padding(
                        padding: const EdgeInsets.only(left:80.0)),
                    new Radio(
                      value: 4,
                      groupValue: model.selectedRadio,
                      activeColor: AppStyles.blueColor,
                      onChanged: (val){
                        model.setSelectedRadio(val);
                        model.setShowCustom(true);
                      },

                    ),
                    new Text(
                      'Custom Range',
                      style: new TextStyle(fontSize: 10.0),
                    ),
                  ],
                ),
              ],
            )

          );
        });
  }

  Widget customDatePicker(){
    return ScopedModelDescendant<ReportFragmentViewModel>(builder: (context, child, model) {
      return new Container(
        child: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Expanded(child: startDatePicker(),),
                /*new Padding(padding: EdgeInsets.only(left: 8.0, right: 8.0)),
                new Expanded(child: startTimePicker())*/
              ],
            ),
            new Row(
              children: <Widget>[
                new Expanded(child: endDatePicker(),),
               /* new Padding(padding: EdgeInsets.only(left: 8.0, right: 8.0)),
                new Expanded(child: endTimePicker())*/
              ],
            )
          ],
        ),
      );

    },);
  }

  Widget startDatePicker(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model){
        return
          new Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey.withOpacity(0.5),
              width: 1.0,
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
          margin: const EdgeInsets.symmetric(
              vertical: 10.0),
          padding: EdgeInsets.only(left:8.0),
          child: new Column(children: <Widget>[
            DateTimeField(
              format: model.format,
             onShowPicker: (context, currentValue) {
               if(currentValue!=null)
                 print("Vall : "+currentValue.toString());
               return showDatePicker(
                 context: context,
                 firstDate: DateTime(1900),
                 initialDate: currentValue ?? DateTime.now(),
                 lastDate: DateTime(2100),

               );
             },
              onChanged: (dt) => setState(() => model.startDate = dt.toString()),
            ),
          ]),
        );
      },
    );
  }

  Widget startTimePicker(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model){
        return
          new Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey.withOpacity(0.5),
              width: 1.0,
            ),
            borderRadius: BorderRadius.circular(5.0),
          ),
          margin: const EdgeInsets.symmetric(
              vertical: 10.0),
          padding: EdgeInsets.only(left:8.0),
          child: new Column(
            children: <Widget>[
              DateTimeField(
                onSaved: (startTime) {
                  if (startTime==null) {
                    return "Please enter start time";
                  }
                  else {
                    model.startTime = startTime.toString();
                  }
                  return null;
                },

                onShowPicker: (context, currentValue) async {
                  final time = await showTimePicker(
                    context: context,
                    initialTime: TimeOfDay.fromDateTime(currentValue ?? DateTime.now()),
                  );
                  return DateTimeField.convert(time);
                },
                format: model.time_format,
                onChanged: (time) => setState(() => model.startTime = time.toString()),
              ),
              ]
          ),
        );
      },
    );
  }

  Widget endDatePicker(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model){
        return
          new Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey.withOpacity(0.5),
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(5.0),
            ),
            margin: const EdgeInsets.symmetric(
                vertical: 10.0),
            padding: EdgeInsets.only(left:8.0),
            child: new Column(
                children: <Widget>[
                  DateTimeField(
                    onSaved: (endDate) {
                      if (endDate==null) {
                        return "Please enter end Date";
                      }
                      else {
                        model.endDate = endDate.toString();
                      }
                      return null;
                    },
                    format: model.format,
                    onShowPicker: (context, currentValue) {
                      if(currentValue!=null)
                      print("Vall : "+currentValue.toString());
                      return showDatePicker(
                          context: context,
                          firstDate: DateTime(1900),
                          initialDate: currentValue ?? DateTime.now(),
                          lastDate: DateTime(2100),

                      );
                    },
                    onChanged: (dt) => setState(() => model.endDate = dt.toString()),
                  ),
                ]
            ),
          );
      },
    );
  }

  Widget endTimePicker(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model) {
        return
          new Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey.withOpacity(0.5),
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(5.0),
            ),
            margin: const EdgeInsets.symmetric(
                vertical: 10.0),
            padding: EdgeInsets.only(left: 8.0),
            child: new Column(
                children: <Widget>[
                  DateTimeField(
                    onSaved: (endTime) {
                      if (endTime==null) {
                        return "Please enter end time";
                      }
                      else {
                        model.endTime = endTime.toString();
                      }
                      return null;
                    },

                    onShowPicker: (context, currentValue) async {
                      print("Vall : "+currentValue.toString());
                      final time = await showTimePicker(
                        context: context,
                        initialTime: TimeOfDay.fromDateTime(
                            currentValue ?? DateTime.now()),
                      );

                      return DateTimeField.convert(time);
                    },
                    format: model.time_format,
                    onChanged: (time) => setState(() => model.endTime = time.toString()),
                  ),
                ]
            ),
          );
      },
    );
  }


  Widget dropDownButton(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model) {
        return new Container(
          child: DropdownButton<DeviceData>(
            hint: Text('Please choose a entity'), // Not necessary for Option 1
            value: model.deviceData,
            onChanged: (DeviceData deviceData) {
              setState(() {
                model.deviceData = deviceData;
              });
            },
            items: model.deviceList.map((deviceData) => DropdownMenuItem<DeviceData>(
              child: Text(deviceData.entityName,
              style: new TextStyle(color: Colors.black),),
              value: deviceData,
            ))
                .toList(),
          ),
        );
      },);
  }


  Widget viewPDF(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder:(context, child, model){
      return new Container(
        padding: EdgeInsets.only(top:30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                buttonEmail(),
                new Padding(padding: EdgeInsets.only(left: 60.0)),
                buttonViewPDF()
              ],
            ),
          ],
        ),
      );
    });
  }

  Widget buttonEmail(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model) {
        return new Row(
          mainAxisAlignment: MainAxisAlignment.end,

          children: <Widget>[
            new RaisedButton(
              onPressed: () {

              },
              textColor: Colors.white,
              color: AppStyles.blueColor,
              padding: const EdgeInsets.all(8.0),
              child: Text('Email',
                  style: TextStyle(fontFamily: 'Roboto', color: Colors.white)),
            ),
          ],
        );
      },
    );
  }

  Widget buttonViewPDF(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model) {
        return new Row(
          mainAxisAlignment: MainAxisAlignment.end,

          children: <Widget>[
            new RaisedButton(
              onPressed: () {
              model.deviceId = model.deviceData.deviceId.toString();
              model.isLoading? loader(): model.getReportData(model.startDate, model.endDate);
              },
              textColor: Colors.white,
              color: AppStyles.blueColor,
              padding: const EdgeInsets.all(8.0),
              child: Text('View PDF',
                  style: TextStyle(fontFamily: 'Roboto', color: Colors.white)),
            ),
          ],
        );
      },
    );
  }

  Widget openPdf(){
    return ScopedModelDescendant<ReportFragmentViewModel>(
      builder: (context, child, model){
        return new Container(
          child: new Column(
            children: <Widget>[

            ],
          ),
        );
      },
    );
  }

  Widget loader(){
    return new Container(
      padding: EdgeInsets.all(8.0),
      child: new ColorLoader(),
    );
  }

}