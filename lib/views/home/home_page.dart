import 'package:coolsense_manager_app/view_models/home/home_view_model.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {
  HomeViewModel _model;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _model = new HomeViewModel();
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ScopedModel<HomeViewModel>(
        model: _model,
        child: new Scaffold(
          backgroundColor: Colors.grey,
          body: body(),
          bottomNavigationBar: bottomNavigationBar(),
        )
    );
  }

  Widget body() {
    return new ScopedModelDescendant<HomeViewModel>(
        builder: (context, child, model) {
          return model.fragments[model.bottomNavigationBarIndex];
        }
    );
  }

  Widget bottomNavigationBar() {
    return new ScopedModelDescendant<HomeViewModel>(
        builder: (context, child, model) {
          return new BottomNavigationBar(
            items: [
              new BottomNavigationBarItem(
                icon: new Icon(Icons.dashboard),
                title: new Text('Home',
                    style: TextStyle( fontFamily : 'Roboto'),),
              ),

              new BottomNavigationBarItem(
                title: new Text('Alerts',
                style: TextStyle( fontFamily : 'Roboto'),),
                icon: new Stack(
                    children: <Widget>[
                      new Icon(Icons.notifications),
                      new Positioned(  // draw a red marble
                        top: 0.0,
                        right: 0.0,
                        child: new Icon(Icons.brightness_1, size: 8.0,
                            color: Colors.redAccent),
                      ),
                    ]
                ),
              ),

              new BottomNavigationBarItem(
                icon: new Icon(Icons.assignment),
                title: new Text('Reports',
                    style: TextStyle( fontFamily : 'Roboto')),
              ),

              new BottomNavigationBarItem(
                icon: new Icon(Icons.person),
                title: new Text('Profile',
                    style: TextStyle( fontFamily : 'Roboto')),
              ),

            ],

            type: BottomNavigationBarType.fixed,
            currentIndex: model.bottomNavigationBarIndex,
            onTap: (index)=> model.bottomNavigationBarIndex = index,

          );
        }
    );
  }

}
