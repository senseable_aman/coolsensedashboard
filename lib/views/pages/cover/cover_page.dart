import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/view_models/cover/cover_view_model.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
class CoverPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new CoverPageState();
}
class CoverPageState  extends State<CoverPage> {
  CoverViewModel _model;
  @override
  // ignore: must_call_super
  void initState() {
    _model = new CoverViewModel();
    _model.getIsLogin();

  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: AppStyles.blueColor,
      body: body(),
    );
  }
  Widget body(){
    _model.buildContext(context);
    _model.startTimer();
    return new ScopedModel<CoverViewModel>(
        model: _model,
        child: new Container(
          width: double.infinity,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              new Image.asset('images/white_transparent_logo.png',
                height: 80,
              ),
              new Padding(padding: EdgeInsets.only(top: 10.0)),
              new Container(
                margin: const EdgeInsets.all(15.0),
                padding: const EdgeInsets.all(3.0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.white)
                ),
                child :new Center(
                  child: Text("CoolSense\u2122",
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.white,  fontFamily : 'Eurostib'),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}