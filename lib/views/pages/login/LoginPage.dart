import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:coolsense_manager_app/loader/color_loader.dart';
import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/view_models/login/login_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginPage extends StatefulWidget {
static String tag = 'login-page';
@override
_LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  LoginViewModel _loginModel;
  final _formKey = GlobalKey<FormState>();
  StreamSubscription<ConnectivityResult> _connectivitySubscription;



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loginModel = new LoginViewModel();
    _connectivitySubscription =
        new Connectivity().onConnectivityChanged.listen((
            ConnectivityResult result) {
          // Got a new connectivity status!
          _loginModel.checkInternetConnection(result);
        });
    _loginModel.initializeFireBase();
    _loginModel.register();
    _loginModel.getMessage();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ScopedModel<LoginViewModel>(
        model: _loginModel,
        child: Scaffold(
      appBar: appBar(),
      body: body(),
    )
    );
  }
  Widget appBar() {
    return  new AppBar(
      backgroundColor: AppStyles.blueColor,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                  'Login', style: TextStyle(fontFamily: 'Roboto'))),
        ],
      ),
    );
  }

  Widget logo(){
    return new Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
      child: Column(
        children: <Widget>[
          Image.asset(
            'images/rounded_logo.png',
            fit: BoxFit.fitWidth,
            width: 120,),
          Text("CoolSense",
            style: TextStyle(fontFamily: 'Eurostib',
                fontWeight: FontWeight.bold,
                fontSize: 32.0),)
        ],
      ),
    );
  }

  Widget loginForm(){
    return new ScopedModelDescendant<LoginViewModel>(
        builder: (context, child, model){
          return new Form(
              key: _formKey,
              child: new Column(
                children: <Widget>[
                  new Container(
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.grey.withOpacity(0.5),
                          width: 1.0,
                        ),
                        borderRadius: BorderRadius.circular(32.0),
                      ),
                      margin: const EdgeInsets.symmetric(
                          vertical: 10.0),
                      child: Row(
                        children: <Widget>[
                          new Padding(
                            padding:
                            EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 15.0),
                            child: Icon(
                              Icons.person_outline,
                              color: Colors.grey,
                            ),
                          ),
                          Container(
                            height: 30.0,
                            width: 1.0,
                            color: Colors.grey.withOpacity(0.5),
                            margin: const EdgeInsets.only(
                                left: 00.0, right: 10.0),
                          ),
                          new Expanded(
                            child: TextFormField(
                              //initialValue : 'user@pharma.com',
                              validator: (userNameValue) {
                                if (userNameValue.isEmpty) {
                                  return "Please enter username";
                                }
                                else{
                                  model.username = userNameValue;
                                }
                                return null;
                              },
                              keyboardType: TextInputType.emailAddress,
                              autofocus: false,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                                hintText: 'Enter your email',
                                hintStyle: TextStyle(fontFamily: 'Roboto', color: Colors.grey),
                              ),
                            ),
                          )
                        ],
                      )
                  ),
                  new Container(
                    decoration: BoxDecoration(
                      border: Border.all(
                        color: Colors.grey.withOpacity(0.5),
                        width: 1.0,
                      ),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    margin:
                    const EdgeInsets.symmetric(
                        vertical: 10.0),
                    child: Row(
                      children: <Widget>[
                        new Padding(
                          padding:
                          EdgeInsets.symmetric(
                              vertical: 10.0, horizontal: 15.0),
                          child: Icon(
                            Icons.lock_open,
                            color: Colors.grey,
                          ),
                        ),
                        Container(
                          height: 30.0,
                          width: 1.0,
                          color: Colors.grey.withOpacity(0.5),
                          margin: const EdgeInsets.only(
                              left: 00.0, right: 10.0),
                        ),
                        new Expanded(
                          child: TextFormField(
                            //initialValue : 'test',
                            validator: (userPassValue) {
                              if (userPassValue.isEmpty) {
                                return "Please enter password";
                              }
                              else{
                                model.password = userPassValue;
                              }
                              return null;
                            },
                            autofocus: false,
                            //initialValue: 'some password',
                            obscureText: true,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Enter your password',
                              hintStyle: TextStyle(
                                  fontFamily: 'Roboto', color: Colors.grey),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(height: 24.0),
                  new Padding(
                    padding: EdgeInsets.symmetric(vertical: 16.0),
                    child:
                    RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(24),
                      ),
                      onPressed: () {
                        if (_formKey.currentState.validate()){
                          model.doLogin(context,model.username, model.password);
                        }
                      },
                      padding: EdgeInsets.all(12),
                      color: AppStyles.blueColor,
                      child: Text('Log In', style: TextStyle(fontFamily:'Roboto', color: Colors.white)),
                    ),
                  ),
                ],
              )
          );
        }
        );
  }
  Widget body() {
    return ScopedModelDescendant<LoginViewModel>(builder: (context, child, model){
      return  new Container(
        padding: EdgeInsets.only(left: 24.0, right: 24.0),
        child: SingleChildScrollView(
          child:new Center(
              child: new Column(
                children: <Widget>[
                  logo(),
                  loginForm(),
                  model.isLoading ? loader() : new Container()
                ],
              )
          ) ,
        ),
      );
    },);

  }


  Widget loader(){
    return new Container(
      padding: EdgeInsets.all(8.0),
      child: new ColorLoader(),
    );

  }

  @override
  void dispose() {
    // TODO: implement dispose
    _connectivitySubscription.cancel();
    super.dispose();
  }


}