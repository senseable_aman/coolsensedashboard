import 'dart:async';

import 'package:charts_flutter/flutter.dart' as charts;
import 'package:coolsense_manager_app/loader/color_loader.dart';
import 'package:coolsense_manager_app/utils/app_styles.dart';
import 'package:coolsense_manager_app/view_models/entity_details/entity_detail_view_model.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flutter/gestures.dart';

class EntityDetailPage extends StatefulWidget{

  String name;
  EntityDetailPage({@required this.name});

  @override
  State<StatefulWidget> createState() => EntityDetailPageData();

}

class EntityDetailPageData extends State<EntityDetailPage> {
  EntityDetailViewModel _entityDetailViewModel;
  Completer<GoogleMapController> _controller = Completer();


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _entityDetailViewModel = new EntityDetailViewModel();
    _entityDetailViewModel.getToken();
    _entityDetailViewModel.getLocationId();
    _entityDetailViewModel.getEntityId();
    _entityDetailViewModel.setCustomMapPin();

    new Timer(const Duration(milliseconds: 400), () {
      setState(() {
        _entityDetailViewModel.addDeviceGraphData();
        _entityDetailViewModel.getGpsDeviceData();
        _entityDetailViewModel.getLocation();
      });
    });

  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new ScopedModel<EntityDetailViewModel>(
      model: _entityDetailViewModel,
      child: new Scaffold(
        appBar: appBar(),
        body: body(),
      ),
    );
  }

  Widget appBar() {
    return new AppBar(
        backgroundColor: AppStyles.blueColor,
        title: new ScopedModelDescendant<EntityDetailViewModel>(
            builder: (context, child, model) {
              return new Row(
                children: <Widget>[
                  new Image.asset('images/white_transparent_logo.png',
                    height: 30,
                  ),
                  new Flexible(
                      child: new Text(widget.name,
                        style: new TextStyle(
                            color: Colors.white,
                            fontSize: 18.0,
                            fontFamily : 'Roboto'
                        ),
                        overflow: TextOverflow.ellipsis,
                      )
                  )
                ],
              );
            }
        )
    );
  }

  Widget body() {
    return SingleChildScrollView(
      child: new ScopedModelDescendant<EntityDetailViewModel>(
          builder: (context, child, model) {
            return  new Column(
              children: <Widget>[
                new Padding(padding: EdgeInsets.all(5.0)),
                model.isLoading ? loader() :  model.tempList.isNotEmpty ?   new Container(
                  color: AppStyles.lightGrayColor,
                  child: new Column(
                    children: <Widget>[
                      model.tempHumidGraphData.type=='TH'?new Container(
                        child: new Column(
                          children: <Widget>[
                            new Padding(padding: EdgeInsets.only(top: 5.0)),
                            toggleButton(),
                            tempData(),
                            humidityData(),
                          ],
                        ),
                      ):new Container(
                        child: new Column(
                          children: <Widget>[
                            new Padding(padding: EdgeInsets.only(top: 5.0)),
                            toggleButton(),
                            tempData()
                          ],
                        ),
                      ),
                    ],
                  ),
                ) :  model.gasMeterDataList.isNotEmpty ?new Container(
                  color: AppStyles.lightGrayColor,
                  child: new Column(
                    children: <Widget>[
                      electricityData()
                    ],
                  ),
                ): model.gpsDataList.isNotEmpty ? new Container(
                    child: new Column(
                      children: <Widget>[
                        googleMap(model.currentLocation.latitude, model.currentLocation.longitude),
                        new Divider(),
                        gpsDataList()
                      ],
                    )
                ): loader()
              ],
            );
          }),
    );
  }


  Widget toggleButton(){
    return new ScopedModelDescendant<EntityDetailViewModel>(builder: (context, child, model){
      return new Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            ToggleButtons(
              //borderColor: Colors.blueGrey,
              fillColor: AppStyles.lightBlueColor,
              borderWidth: 1,
             // selectedBorderColor: Colors.blueGrey,
              selectedColor: Colors.grey,
              borderRadius: BorderRadius.circular(5.0),
              children: <Widget>[
                Container(height:30,
                    //width: (MediaQuery.of(context).size.width - 24)/2,
                    padding: EdgeInsets.all(5.0),
                    child: new Row(mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text("24 Hour",style: TextStyle(color: model.interval== '24 hour'?Colors.white:Colors.black),)
                      ],
                    )),
                Container(height:30,
                    padding: EdgeInsets.all(5.0),
                    //width: (MediaQuery.of(context).size.width - 24)/2,
                    child: new Row(mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text("Weekly",style: TextStyle(color: model.interval== '7 day'?Colors.white:Colors.black))
                      ],
                    )),
              ],
              onPressed: (int index) {
                setState(() {
                  for (int i = 0; i < model.isSelected.length; i++) {
                    model.isSelected[i] = i == index;
                    if(index==0){
                      model.interval = "24 hour";
                      model.addDeviceGraphData();
                    }
                    else if(index==1){
                      model.interval = "7 day";
                      model.addDeviceGraphData();
                    }
                  }
                  print("interval1 : "+model.interval);
                });
              },
              isSelected: model.isSelected,
            ),
          ],
        ),
      );
    });
  }


  Widget tempData(){
    return new ScopedModelDescendant<EntityDetailViewModel>(builder: (context, child, model){
      return  new Container(
        height: 270,
        margin: EdgeInsets.all(8.0),
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            /* boxShadow: <BoxShadow>[
              BoxShadow(
                  spreadRadius: 10.0,
                  color: Colors.black54,
                  blurRadius: 15.0,
                  offset: Offset(0.0, 0.75)
              )
            ],*/
            color: Colors.white
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Temperature : ",
                  style: TextStyle(fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.tempList==null ? '' : model.tempList[0].value.toString()+"°C",
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Padding(padding: EdgeInsets.all(3.0)),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Average Temperature : ",
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.tempHumidGraphData==null ? '' : model.tempHumidGraphData.avgtemprature.toStringAsFixed(2)+"°C",
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      color: AppStyles.redColor,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Min/Max Temperature : ",
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.tempHumidGraphData==null ? '' :model.tempHumidGraphData.map.minmaxtemp.minValue.toString()+"/"+model.tempHumidGraphData.map.minmaxtemp.maxValue.toString()+ "°C",
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      color: AppStyles.lightBlueColor,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Column(
              children: <Widget>[
                Container(
                  width: 500.0,
                  height: 200.0,
                  child : tempChart(),
                ),
              ],
            ),
          ],
        ),
      );
    });
  }


  Widget tempChart() {
    print("min ; "+_entityDetailViewModel.tempHumidGraphData.map.minmaxtemp.minValue.toString()+" : "+_entityDetailViewModel.tempHumidGraphData.map.minmaxtemp.maxValue.toString());
    return ScopedModelDescendant<EntityDetailViewModel>(
      builder: (context, child, model) {
        return new charts.TimeSeriesChart(model.createTempGraphData,
            customSeriesRenderers: [
              new charts.LineRendererConfig(
                // ID used to link series to this renderer.
                  customRendererId: 'customArea',
                  dashPattern:  [4,4],

                  //includeArea: true,
                  stacked: true),
            ],
          domainAxis: new charts.DateTimeAxisSpec(
              tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
                  day: new charts.TimeFormatterSpec(
                      format: 'hh:mm', transitionFormat: 'dd MMM'))),
          animate: true,
          defaultInteractions: false,
          primaryMeasureAxis: new charts.NumericAxisSpec(
              tickProviderSpec: new charts.BasicNumericTickProviderSpec(zeroBound: false),
              showAxisLine: true,
              renderSpec: new charts.GridlineRendererSpec(
                  lineStyle: charts.LineStyleSpec(thickness: 1, dashPattern: [4,4]),
                  axisLineStyle: charts.LineStyleSpec(thickness: 1),

              )),
          behaviors: [ /*new charts.LinePointHighlighter(
            //showHorizontalFollowLine: charts.LinePointHighlighterFollowLineType.none,
           */ /* showVerticalFollowLine: charts.LinePointHighlighterFollowLineType.nearest*/ /*),*/
            new charts.ChartTitle("Time",
              behaviorPosition: charts.BehaviorPosition.bottom,
              titleStyleSpec: charts.TextStyleSpec(fontSize: 15,
                color: charts.ColorUtil.fromDartColor(AppStyles.blueColor),),
              titleOutsideJustification: charts.OutsideJustification
                  .middleDrawArea,
            ),
            /*new charts.ChartTitle("Temperature(°C)",behaviorPosition: charts.BehaviorPosition.start,
              titleStyleSpec: charts.TextStyleSpec(fontSize: 15,color: charts.ColorUtil.fromDartColor(AppStyles.blueColor)),
              titleOutsideJustification: charts.OutsideJustification.middleDrawArea),*/
            new charts.SelectNearest(
                eventTrigger: charts.SelectionTrigger.tapAndDrag),
            new charts.DomainHighlighter()
          ],);
      },);

  }

/*
*
* humidity data and graph
*
* */
  Widget humidityData(){
    return new ScopedModelDescendant<EntityDetailViewModel>(builder: (context, child, model){
      return  new Container(
        height: 280,
        margin: EdgeInsets.all(8.0),
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            /*boxShadow: <BoxShadow>[
            BoxShadow(
                spreadRadius: 10.0,
                color: Colors.black54,
                blurRadius: 15.0,
                offset: Offset(0.0, 0.75)
            )
          ],*/
            color: Colors.white
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Humidity : ",
                  style: TextStyle(fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.humidDataList==null ? '' : model.humidDataList[0].value.toStringAsFixed(2)+"%",
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Padding(padding: EdgeInsets.all(3.0)),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Average Humidity : ",
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.tempHumidGraphData==null ? '' : model.tempHumidGraphData.avghumidity1.toStringAsFixed(2)+"%",
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      color: AppStyles.redColor,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Min/Max Humidity : ",
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.tempHumidGraphData==null ? '' :model.tempHumidGraphData.map.minmaxHumidity.minValue.toString()+"/"+model.tempHumidGraphData.map.minmaxHumidity.maxValue.toString()+"%",
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      color: AppStyles.lightBlueColor,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Padding(padding: EdgeInsets.all(3.0)),
            new Column(
              children: <Widget>[
                Container(
                  width: 500.0,
                  height: 200.0,
                  child : humidChart(),
                ),
              ],
            ),
          ],
        ),
      );
    });
  }

  Widget humidChart() {
    return ScopedModelDescendant<EntityDetailViewModel>(
      builder: (context, child, model) {
        return new charts.TimeSeriesChart(model.createHumidGraphData,
          customSeriesRenderers: [
            new charts.LineRendererConfig(
              // ID used to link series to this renderer.
                customRendererId: 'customArea',
                dashPattern:  [4,4],

                //includeArea: true,
                stacked: true),
          ],
          domainAxis: new charts.DateTimeAxisSpec(
              tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
                  day: new charts.TimeFormatterSpec(
                      format: 'hh:mm', transitionFormat: 'dd MMM'))),
          animate: true,
          defaultInteractions: false,
          primaryMeasureAxis: new charts.NumericAxisSpec(
              tickProviderSpec: new charts.BasicNumericTickProviderSpec(zeroBound: false),
              showAxisLine: true,
             renderSpec: new charts.GridlineRendererSpec(
        lineStyle: charts.LineStyleSpec(
        dashPattern: [4, 4],)
              )),
          behaviors: [/*new charts.LinePointHighlighter(
            //showHorizontalFollowLine: charts.LinePointHighlighterFollowLineType.none,
           *//* showVerticalFollowLine: charts.LinePointHighlighterFollowLineType.nearest*//*),*/
            new charts.ChartTitle("Time",
              behaviorPosition: charts.BehaviorPosition.bottom,
              titleStyleSpec: charts.TextStyleSpec(fontSize: 15, color: charts.ColorUtil.fromDartColor(AppStyles.blueColor),),
              titleOutsideJustification: charts.OutsideJustification.middleDrawArea,
            ),
            /*new charts.ChartTitle("Temperature(°C)",behaviorPosition: charts.BehaviorPosition.start,
              titleStyleSpec: charts.TextStyleSpec(fontSize: 15,color: charts.ColorUtil.fromDartColor(AppStyles.blueColor)),
              titleOutsideJustification: charts.OutsideJustification.middleDrawArea),*/
            new charts.SelectNearest(eventTrigger: charts.SelectionTrigger.tapAndDrag),new charts.DomainHighlighter()
          ],);
      },);
  }

  Widget electricityData(){
    return new ScopedModelDescendant<EntityDetailViewModel>(builder: (context, child, model){
      return  new Container(
        height: 300,
        margin: EdgeInsets.all(8.0),
        padding: EdgeInsets.all(8.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(8.0)),
            /* boxShadow: <BoxShadow>[
            BoxShadow(
                spreadRadius: 10.0,
                color: Colors.black54,
                blurRadius: 15.0,
                offset: Offset(0.0, 0.75)
            )
          ],*/
            color: Colors.white
        ),
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Gas Meter",
                  style: TextStyle(fontWeight: FontWeight.bold,
                      fontSize: 15.0,
                      fontFamily: 'Roboto'),
                ),
                /* new Text(model.tempList1==null ? '' : model.tempList1[0].value.toString()+"°C",
                textAlign: TextAlign.end,
                style: TextStyle(fontWeight: FontWeight.bold,
                    fontSize: 15.0,
                    fontFamily: 'Roboto'),
              ),*/
              ],
            ),
            new Padding(padding: EdgeInsets.all(3.0)),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Total Consumption : ",
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.gasMeterDataList1==null ? '' : model.totalConsumed().toString(),
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Current Consumption : ",
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.gasMeterDataList1==null ? '' :model.gasMeterDataList1[model.gasMeterDataList1.length-1].value.toString(),
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new Text("Current Reading : ",
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
                new Text(model.gasMeterDataList==null ? '' :model.gasMeterDataList[model.gasMeterDataList1.length-1].value.toString(),
                  textAlign: TextAlign.end,
                  style: TextStyle(fontWeight: FontWeight.normal,
                      fontSize: 12.0,
                      fontFamily: 'Roboto'),
                ),
              ],
            ),
            new Padding(padding: EdgeInsets.all(3.0)),
            new Column(
              children: <Widget>[
                Container(
                  width: 500.0,
                  height: 200.0,
                  child : electricityChart(),
                ),
              ],
            ),
          ],
        ),
      );
    });
  }

  Widget electricityChart() {
    return ScopedModelDescendant<EntityDetailViewModel>(
      builder: (context, child, model) {
        return model.createGasMeterGraphData == null ? '' : new charts
            .TimeSeriesChart(model.createGasMeterGraphData, animate: true,
          defaultRenderer: new charts.BarRendererConfig<DateTime>(),
          primaryMeasureAxis: new charts.NumericAxisSpec(
              renderSpec: new charts.SmallTickRendererSpec(
                // Tick and Label styling here.
              )),
          secondaryMeasureAxis: new charts.NumericAxisSpec(
              renderSpec: new charts.SmallTickRendererSpec(
                  labelStyle: new charts.TextStyleSpec(
                      fontSize: 18, // size in Pts.
                      color: charts.MaterialPalette.black),
                  lineStyle: new charts.LineStyleSpec(
                      color: charts.MaterialPalette.black))),
          domainAxis: new charts.DateTimeAxisSpec(
            tickProviderSpec: charts.DayTickProviderSpec(increments: [1]),
            tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
              day: new charts.TimeFormatterSpec(
                  format: 'd',
                  transitionFormat: 'd'
              ),
            ),
          ),
          // If default interactions were removed, optionally add select nearest
          // and the domain highlighter that are typical for bar charts.
          behaviors: [
            new charts.SelectNearest(
                eventTrigger: charts.SelectionTrigger.tapAndDrag),
            new charts.DomainHighlighter(),
            new charts.ChartTitle(model.formatter1.format(
                DateTime.parse(model.gasMeterDataList[0].time_captured)),
              behaviorPosition: charts.BehaviorPosition.bottom,
              titleStyleSpec: charts.TextStyleSpec(fontSize: 15,
                color: charts.ColorUtil.fromDartColor(AppStyles.blueColor),),
              titleOutsideJustification: charts.OutsideJustification
                  .middleDrawArea,
            ),
          ],);
      },);
  }

  Widget googleMap(double lat, double long){
    return new ScopedModelDescendant<EntityDetailViewModel>(builder: (context, child, model) {
      return new Container(
          height: 300,
          child: new GoogleMap(
            myLocationEnabled: true,
            markers: Set.of(model.markers),
            mapType: MapType.normal,
            initialCameraPosition: CameraPosition(
                target: LatLng(lat, long),
                zoom: 12.00),
            gestureRecognizers: Set()
              ..add(
                  Factory<PanGestureRecognizer>(() => PanGestureRecognizer()))
              ..add(
                Factory<VerticalDragGestureRecognizer>(
                        () => VerticalDragGestureRecognizer()),
              )
              ..add(
                Factory<HorizontalDragGestureRecognizer>(
                        () => HorizontalDragGestureRecognizer()),
              )
              ..add(
                Factory<ScaleGestureRecognizer>(
                        () => ScaleGestureRecognizer()),
              ),
            onMapCreated: (GoogleMapController controller) {
              _controller.complete(controller);
            },
          ),
      );
    });
  }

  Widget gpsDataList(){
    return new ScopedModelDescendant<EntityDetailViewModel>(builder: (context, child, model){
      return new Container(
         child:  new  Card(
           child: Column(
             children: <Widget>[
               new Text("Locations",
                   style: TextStyle(fontWeight: FontWeight.bold, color: AppStyles.blueColor),),
               new Divider(),
               new ListView.separated(
                 shrinkWrap: true,
                 padding: EdgeInsets.all(10.0),
                 itemCount: model.address == null ? 0 : model.address.length,
                 separatorBuilder:(context, index) {
                   return new Divider();
                 },
                 itemBuilder: (context, index) {
                   return new GestureDetector(
                     onTap: () {
                       print("lat : "+ model.address[index].latitude.toString()+" : "+model.address[index].longitude.toString());
                       model.addMarker(index);
                      // googleMap(model.address[index].latitude, model.address[index].longitude);
                     },
                     child: new ListTile(
                       leading: Icon(Icons.location_on),
                       title: Text(model.address!=null ? model.address[index].addresses:""),
                       subtitle: Text(model.address!=null ? model.address[index].timeCaptured:"",
                       style: TextStyle(fontStyle: FontStyle.italic, color: AppStyles.blueColor, fontSize: 10.0),),
                     ),
                   );
                 },
               ),
             ],
           ),
         ),
      );
    });
  }

  Widget loader(){
    return new Container(
      padding: EdgeInsets.all(8.0),
      child: new ColorLoader(),
    );
  }
}
