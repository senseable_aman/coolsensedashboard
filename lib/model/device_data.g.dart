// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'device_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DeviceData _$DeviceDataFromJson(Map<String, dynamic> json) {
  return DeviceData(
      deviceId: json['deviceId'] as int,
      deviceName: json['deviceName'] as String,
      entityName: json['entityName'] as String,
      temp: Temp.fromJson(json['temp'] as Map<String, dynamic>),
      type: json['type'] as String,
      gasMeter: GasMeter.fromJson(json['gasMeter'] as Map<String, dynamic>),
      humidity: Temp.fromJson(json['humidity'] as Map<String, dynamic>),
      gpsCompVo: GpsCompVo.fromJson(json['gpsCompVo'] as Map<String, dynamic>));
}

Map<String, dynamic> _$DeviceDataToJson(DeviceData instance) =>
    <String, dynamic>{
      'deviceId': instance.deviceId,
      'deviceName': instance.deviceName,
      'entityName': instance.entityName,
      'type': instance.type,
      'temp': instance.temp,
      'gasMeter': instance.gasMeter,
      'humidity': instance.humidity,
      'gpsCompVo': instance.gpsCompVo
    };
