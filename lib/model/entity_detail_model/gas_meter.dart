import 'package:json_annotation/json_annotation.dart';
part 'gas_meter.g.dart';

@JsonSerializable(nullable: false)
class GasMeter{
  //int id;
  double value;
  //int hour;
  String time_captured;
 // double maxTemp;
 // double temp;
 // double humidity;

  GasMeter(
      {//this.id,
        this.value,
       // this.hour,
        this.time_captured,
       // this.maxTemp,
       // this.temp,
       // this.humidity
   });

  factory GasMeter.fromJson(Map<String, dynamic> json) => _$GasMeterFromJson(json);

  Map<String, dynamic> toJson() => _$GasMeterToJson(this);
}


