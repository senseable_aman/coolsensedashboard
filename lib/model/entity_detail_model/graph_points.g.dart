// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'graph_points.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GraphPoints _$GraphPointsFromJson(Map<String, dynamic> json) {
  return GraphPoints(
      recTime: json['recTime'] as String,
      value: (json['value'] as num).toDouble());
}

Map<String, dynamic> _$GraphPointsToJson(GraphPoints instance) =>
    <String, dynamic>{'recTime': instance.recTime, 'value': instance.value};
