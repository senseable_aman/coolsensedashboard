import 'package:json_annotation/json_annotation.dart';
part 'gps_address_list.g.dart';

@JsonSerializable(nullable: false)
class GPSAddressList {
  String addresses;
  String timeCaptured;
  double latitude;
  double longitude;

  GPSAddressList({
    this.addresses,
    this.timeCaptured,
    this.latitude,
    this.longitude
  });

  factory GPSAddressList.fromJson(Map<String, dynamic> json) => _$GPSAddressListFromJson(json);
  Map<String, dynamic> toJson() => _$GPSAddressListToJson(this);
}