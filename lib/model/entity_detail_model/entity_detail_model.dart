
import 'package:json_annotation/json_annotation.dart';
part 'entity_detail_model.g.dart';

@JsonSerializable(nullable: false)
class EntityDetailData{
  int entityId;
  String entityName;
  String entityType;
  int locationId;
  String locationName;
  String locationCity;
  String currentTemperature;
  String currTempRecordedTime;
  String avgTemperature;
  String minTemperature;
  String maxTemperature;
  String currentHumidity;
  String currHumidityRecordedTime;
  String avgHumidity;
  String minHumidity;
  String maxHumidity;
  String currentBattery;
  String currBtryRecordedTime;
  String currDSStatusRecordedTime;
  String avgBattery;
  String minBattery;
  String maxBattery;
  String currentDSStatus;
  double totDistance;
  String parentLocation;
  String electricityCons;
  String fuelConsumption;
  String vehicleStatus;
  String entityStatus;
  String entityHealth;
  String createdDate;
  int noTimeDSStatus;
  int maxDSStatus;
  int totalTimeDSStatus;
  int avgDSStatus;
  int tempAlerts24Hrs;
  int humAlerts24Hrs;
  int btryAlerts24Hrs;
  int maxTimeDSStatus;
  int avgTimeDSStatus;

  EntityDetailData(
      {this.entityId,
        this.entityName,
        this.entityType,
        this.locationId,
        this.locationName,
        this.locationCity,
        this.currentTemperature,
        this.currTempRecordedTime,
        this.avgTemperature,
        this.minTemperature,
        this.maxTemperature,
        this.currentHumidity,
        this.currHumidityRecordedTime,
        this.avgHumidity,
        this.minHumidity,
        this.maxHumidity,
        this.currentBattery,
        this.currBtryRecordedTime,
        this.currDSStatusRecordedTime,
        this.avgBattery,
        this.minBattery,
        this.maxBattery,
        this.currentDSStatus,
        this.totDistance,
        this.parentLocation,
        this.electricityCons,
        this.fuelConsumption,
        this.vehicleStatus,
        this.entityStatus,
        this.entityHealth,
        this.createdDate,
        this.noTimeDSStatus,
        this.maxDSStatus,
        this.totalTimeDSStatus,
        this.avgDSStatus,
        this.tempAlerts24Hrs,
        this.humAlerts24Hrs,
        this.btryAlerts24Hrs,
        this.maxTimeDSStatus,
        this.avgTimeDSStatus});


  factory EntityDetailData.fromJson(Map<String, dynamic> json) => _$EntityDetailDataFromJson(json);
  Map<String, dynamic> toJson() => _$EntityDetailDataToJson(this);
}