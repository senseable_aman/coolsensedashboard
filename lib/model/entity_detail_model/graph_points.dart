import 'package:json_annotation/json_annotation.dart';
part 'graph_points.g.dart';

@JsonSerializable(nullable: false)

class GraphPoints {
  String recTime;
  double value;

  GraphPoints({this.recTime, this.value});

  factory GraphPoints.fromJson(Map<String, dynamic> json) => _$GraphPointsFromJson(json);
  Map<String, dynamic> toJson() => _$GraphPointsToJson(this);
}