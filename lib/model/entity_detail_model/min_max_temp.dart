import 'package:json_annotation/json_annotation.dart';
part 'min_max_temp.g.dart';

@JsonSerializable(nullable: false)
class Minmaxtemp {
  double minValue;
  double maxValue;

  Minmaxtemp({this.minValue, this.maxValue});

  factory Minmaxtemp.fromJson(Map<String, dynamic> json) => _$MinmaxtempFromJson(json);
  Map<String, dynamic> toJson() => _$MinmaxtempToJson(this);
}