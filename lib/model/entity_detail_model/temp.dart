import 'package:json_annotation/json_annotation.dart';
part 'temp.g.dart';

@JsonSerializable(nullable: false)
class Temp{
  int id;
  double value;
  int hour;
  String time_captured;
  double maxTemp;
  double temp;
  double humidity;

  Temp(
      {this.id,
        this.value,
        this.hour,
        this.time_captured,
        this.maxTemp,
        this.temp,
        this.humidity});

  factory Temp.fromJson(Map<String, dynamic> json) => _$TempFromJson(json);

  Map<String, dynamic> toJson() => _$TempToJson(this);
}


