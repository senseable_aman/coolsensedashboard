// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gas_meter.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GasMeter _$GasMeterFromJson(Map<String, dynamic> json) {
  return GasMeter(
      value: (json['value'] as num).toDouble(),
      time_captured: json['time_captured'] as String);
}

Map<String, dynamic> _$GasMeterToJson(GasMeter instance) => <String, dynamic>{
      'value': instance.value,
      'time_captured': instance.time_captured
    };
