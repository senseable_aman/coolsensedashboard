// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gps_address_list.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GPSAddressList _$GPSAddressListFromJson(Map<String, dynamic> json) {
  return GPSAddressList(
      addresses: json['addresses'] as String,
      timeCaptured: json['timeCaptured'] as String,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble());
}

Map<String, dynamic> _$GPSAddressListToJson(GPSAddressList instance) =>
    <String, dynamic>{
      'addresses': instance.addresses,
      'timeCaptured': instance.timeCaptured,
      'latitude': instance.latitude,
      'longitude': instance.longitude
    };
