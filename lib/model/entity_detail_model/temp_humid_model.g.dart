// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'temp_humid_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

TempHumidGraphData _$TempHumidGraphDataFromJson(Map<String, dynamic> json) {
  return TempHumidGraphData(
      deviceId: json['deviceId'] as int,
      deviceName: json['deviceName'] as String,
      entityName: json['entityName'] as String,
      avghumidity1: (json['avghumidity1'] as num).toDouble(),
      avgtemprature: (json['avgtemprature'] as num).toDouble(),
      type: json['type'] as String,
      map: MapData.fromJson(json['map'] as Map<String, dynamic>),
      temp: (json['temp'] as List)
          .map((e) => Temp.fromJson(e as Map<String, dynamic>))
          .toList(),
      humidity: (json['humidity'] as List)
          .map((e) => Humidity.fromJson(e as Map<String, dynamic>))
          .toList(),
      gasMeter: (json['gasMeter'] as List)
          .map((e) => GasMeter.fromJson(e as Map<String, dynamic>))
          .toList());
}

Map<String, dynamic> _$TempHumidGraphDataToJson(TempHumidGraphData instance) =>
    <String, dynamic>{
      'deviceId': instance.deviceId,
      'deviceName': instance.deviceName,
      'entityName': instance.entityName,
      'avghumidity1': instance.avghumidity1,
      'avgtemprature': instance.avgtemprature,
      'type': instance.type,
      'map': instance.map,
      'temp': instance.temp,
      'humidity': instance.humidity,
      'gasMeter': instance.gasMeter
    };
