// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gps_device_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GpsDeviceData _$GpsDeviceDataFromJson(Map<String, dynamic> json) {
  return GpsDeviceData(
      id: json['id'] as int,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      timeCaptured: json['timeCaptured'] as String,
      deviceId: json['deviceId'] as int,
      time_captured1: json['time_captured1'] as String,
      trip_id: json['trip_id'] as String);
}

Map<String, dynamic> _$GpsDeviceDataToJson(GpsDeviceData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'timeCaptured': instance.timeCaptured,
      'deviceId': instance.deviceId,
      'time_captured1': instance.time_captured1,
      'trip_id': instance.trip_id
    };
