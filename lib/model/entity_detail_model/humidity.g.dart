// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'humidity.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Humidity _$HumidityFromJson(Map<String, dynamic> json) {
  return Humidity(
      id: json['id'] as int,
      value: (json['value'] as num).toDouble(),
      hour: json['hour'] as int,
      time_captured: json['time_captured'] as String,
      maxTemp: (json['maxTemp'] as num).toDouble(),
      temp: (json['temp'] as num).toDouble(),
      humidity: (json['humidity'] as num).toDouble());
}

Map<String, dynamic> _$HumidityToJson(Humidity instance) => <String, dynamic>{
      'id': instance.id,
      'value': instance.value,
      'hour': instance.hour,
      'time_captured': instance.time_captured,
      'maxTemp': instance.maxTemp,
      'temp': instance.temp,
      'humidity': instance.humidity
    };
