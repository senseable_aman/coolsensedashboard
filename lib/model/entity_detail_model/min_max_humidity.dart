import 'package:json_annotation/json_annotation.dart';
part 'min_max_humidity.g.dart';

@JsonSerializable(nullable: false)
class MinmaxHumidity {
  double minValue;
  double maxValue;

  MinmaxHumidity({this.minValue, this.maxValue});

  factory MinmaxHumidity.fromJson(Map<String, dynamic> json) => _$MinmaxHumidityFromJson(json);
  Map<String, dynamic> toJson() => _$MinmaxHumidityToJson(this);
}