import 'package:json_annotation/json_annotation.dart';
part 'gps_device_data.g.dart';

@JsonSerializable(nullable: false)
class GpsDeviceData {
  int id;
  double latitude;
  double longitude;
  String timeCaptured;
  int deviceId;
  String time_captured1;
  String trip_id;

  GpsDeviceData({this.id,
    this.latitude,
    this.longitude,
    this.timeCaptured,
    this.deviceId,
    this.time_captured1,
    this.trip_id});

  factory GpsDeviceData.fromJson(Map<String, dynamic> json) => _$GpsDeviceDataFromJson(json);
  Map<String, dynamic> toJson() => _$GpsDeviceDataToJson(this);
}