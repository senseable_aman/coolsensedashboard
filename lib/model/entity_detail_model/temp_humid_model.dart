import 'package:coolsense_manager_app/model/entity_detail_model/map_data.dart';

import 'gas_meter.dart';
import 'humidity.dart';
import 'temp.dart';

import 'package:json_annotation/json_annotation.dart';

import 'graph_points.dart';
part 'temp_humid_model.g.dart';

@JsonSerializable(nullable: false)
class TempHumidGraphData{
  int deviceId;
  String deviceName;
  String entityName;
  double avghumidity1;
  double avgtemprature;
  String type;
  MapData map;
  List<Temp> temp;
  List<Humidity> humidity;
  List<GasMeter> gasMeter;

  TempHumidGraphData(
      {this.deviceId,
        this.deviceName,
        this.entityName,
        this.avghumidity1,
        this.avgtemprature,
        this.type,
        this.map,
        this.temp,
        this.humidity,
        this.gasMeter});

  factory TempHumidGraphData.fromJson(Map<String, dynamic> json) => _$TempHumidGraphDataFromJson(json);
  Map<String, dynamic> toJson() => _$TempHumidGraphDataToJson(this);
}