

import 'package:coolsense_manager_app/model/entity_detail_model/min_max_humidity.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/min_max_temp.dart';
import 'package:json_annotation/json_annotation.dart';
part 'map_data.g.dart';

@JsonSerializable(nullable: false)
class MapData {
  Minmaxtemp minmaxtemp;
  MinmaxHumidity minmaxHumidity;

  MapData({this.minmaxtemp, this.minmaxHumidity});

  factory MapData.fromJson(Map<String, dynamic> json) => _$MapDataFromJson(json);
  Map<String, dynamic> toJson() => _$MapDataToJson(this);
}