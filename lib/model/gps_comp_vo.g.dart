// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'gps_comp_vo.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GpsCompVo _$GpsCompVoFromJson(Map<String, dynamic> json) {
  return GpsCompVo(
      id: json['id'] as int,
      latitude: (json['latitude'] as num).toDouble(),
      longitude: (json['longitude'] as num).toDouble(),
      timeCaptured: json['timeCaptured'] as String);
}

Map<String, dynamic> _$GpsCompVoToJson(GpsCompVo instance) => <String, dynamic>{
      'id': instance.id,
      'latitude': instance.latitude,
      'longitude': instance.longitude,
      'timeCaptured': instance.timeCaptured
    };
