import 'package:json_annotation/json_annotation.dart';
part 'gps_comp_vo.g.dart';

@JsonSerializable(nullable: false)
class GpsCompVo {
  int id;
  double latitude;
  double longitude;
  String timeCaptured;

  GpsCompVo({this.id, this.latitude, this.longitude, this.timeCaptured});

  factory GpsCompVo.fromJson(Map<String, dynamic> json) => _$GpsCompVoFromJson(json);
  Map<String, dynamic> toJson() => _$GpsCompVoToJson(this);

}