import 'package:json_annotation/json_annotation.dart';
part 'humidity.g.dart';

@JsonSerializable(nullable: false)
class Humidity{
  int id;
  double value;
  int hour;
  String timeCaptured;
  double maxTemp;
  double temp;
  double humidity;

  Humidity(
      {this.id,
        this.value,
        this.hour,
        this.timeCaptured,
        this.maxTemp,
        this.temp,
        this.humidity});


  factory Humidity.fromJson(Map<String, dynamic> json) => _$HumidityFromJson(json);
  Map<String, dynamic> toJson() => _$HumidityToJson(this);
}