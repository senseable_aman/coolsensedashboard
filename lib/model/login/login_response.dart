import 'package:json_annotation/json_annotation.dart';
part 'login_response.g.dart';

@JsonSerializable(nullable: false)
class LoginResponse{

  int id;
  String loginId;
  String userName;
  String token;
  bool hasAdminAccess;

  LoginResponse(
      {this.id, this.loginId, this.userName, this.token, this.hasAdminAccess});

  factory LoginResponse.fromJson(Map<String, dynamic> json) => _$LoginResponseFromJson(json);
  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);

}