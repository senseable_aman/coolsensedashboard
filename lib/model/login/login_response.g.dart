// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) {
  return LoginResponse(
      id: json['id'] as int,
      loginId: json['loginId'] as String,
      userName: json['userName'] as String,
      token: json['token'] as String,
      hasAdminAccess: json['hasAdminAccess'] as bool);
}

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'loginId': instance.loginId,
      'userName': instance.userName,
      'token': instance.token,
      'hasAdminAccess': instance.hasAdminAccess
    };
