// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'fcm_token_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FcmTokenResponse _$FcmTokenResponseFromJson(Map<String, dynamic> json) {
  return FcmTokenResponse(
      status: json['status'] as int,
      message: json['message'] as String,
      data: json['data'] as String);
}

Map<String, dynamic> _$FcmTokenResponseToJson(FcmTokenResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data
    };
