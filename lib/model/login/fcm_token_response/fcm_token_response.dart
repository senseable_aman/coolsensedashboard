import 'package:json_annotation/json_annotation.dart';
part 'fcm_token_response.g.dart';

@JsonSerializable(nullable: false)
class FcmTokenResponse{

  int status;
  String message;
  String data;

  FcmTokenResponse({this.status, this.message, this.data});


  factory FcmTokenResponse.fromJson(Map<String, dynamic> json) => _$FcmTokenResponseFromJson(json);
  Map<String, dynamic> toJson() => _$FcmTokenResponseToJson(this);

}