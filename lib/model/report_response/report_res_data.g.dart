// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'report_res_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ReportResData _$ReportResDataFromJson(Map<String, dynamic> json) {
  return ReportResData(
      status: json['status'] as int,
      message: json['message'] as String,
      data: json['data'] as String);
}

Map<String, dynamic> _$ReportResDataToJson(ReportResData instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data
    };
