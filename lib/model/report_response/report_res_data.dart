import 'package:coolsense_manager_app/model/gas_meter.dart';
import 'package:coolsense_manager_app/model/temp.dart';
import 'package:json_annotation/json_annotation.dart';
part 'report_res_data.g.dart';

@JsonSerializable(nullable: false)
class ReportResData{
  int status;
  String message;
  String data;

  ReportResData({this.status, this.message, this.data});

  factory ReportResData.fromJson(Map<String, dynamic> json) => _$ReportResDataFromJson(json);
  Map<String, dynamic> toJson() => _$ReportResDataToJson(this);
}