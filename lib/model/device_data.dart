import 'package:coolsense_manager_app/model/gas_meter.dart';
import 'package:coolsense_manager_app/model/gps_comp_vo.dart';
import 'package:coolsense_manager_app/model/temp.dart';
import 'package:json_annotation/json_annotation.dart';
part 'device_data.g.dart';

@JsonSerializable(nullable: false)
class DeviceData{
  int deviceId;
  String deviceName;
  String entityName;
  String type;
  Temp temp;
  GasMeter gasMeter;
  Temp humidity;
  GpsCompVo gpsCompVo;

  DeviceData({this.deviceId,
    this.deviceName,
    this.entityName,
    this.temp,
    this.type,
    this.gasMeter,
    this.humidity,
    this.gpsCompVo
  });


  factory DeviceData.fromJson(Map<String, dynamic> json) => _$DeviceDataFromJson(json);
  Map<String, dynamic> toJson() => _$DeviceDataToJson(this);
}