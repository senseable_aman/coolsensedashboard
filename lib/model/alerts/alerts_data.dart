import 'package:json_annotation/json_annotation.dart';
part 'alerts_data.g.dart';

@JsonSerializable(nullable: false)
class AlertsData{
  int alertId;
  String alertType;
  String deviceId;
  String deviceName;
  String deviceType;
  String entityName;
  String alertDesc;
  String alertOpenedTime;
  String alertCloseTime;
  String entityId;
  String locationId;

  AlertsData(
      {this.alertId,
        this.alertType,
        this.deviceId,
        this.deviceName,
        this.deviceType,
        this.entityName,
        this.alertDesc,
        this.alertOpenedTime,
        this.alertCloseTime,
        this.entityId,
        this.locationId});

  factory AlertsData.fromJson(Map<String, dynamic> json) => _$AlertsDataFromJson(json);
  Map<String, dynamic> toJson() => _$AlertsDataToJson(this);
}