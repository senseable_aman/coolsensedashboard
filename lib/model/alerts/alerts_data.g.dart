// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'alerts_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlertsData _$AlertsDataFromJson(Map<String, dynamic> json) {
  return AlertsData(
      alertId: json['alertId'] as int,
      alertType: json['alertType'] as String,
      deviceId: json['deviceId'] as String,
      deviceName: json['deviceName'] as String,
      deviceType: json['deviceType'] as String,
      entityName: json['entityName'] as String,
      alertDesc: json['alertDesc'] as String,
      alertOpenedTime: json['alertOpenedTime'] as String,
      alertCloseTime: json['alertCloseTime'] as String,
      entityId: json['entityId'] as String,
      locationId: json['locationId'] as String);
}

Map<String, dynamic> _$AlertsDataToJson(AlertsData instance) =>
    <String, dynamic>{
      'alertId': instance.alertId,
      'alertType': instance.alertType,
      'deviceId': instance.deviceId,
      'deviceName': instance.deviceName,
      'deviceType': instance.deviceType,
      'entityName': instance.entityName,
      'alertDesc': instance.alertDesc,
      'alertOpenedTime': instance.alertOpenedTime,
      'alertCloseTime': instance.alertCloseTime,
      'entityId': instance.entityId,
      'locationId': instance.locationId
    };
