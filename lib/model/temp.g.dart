// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'temp.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Temp _$TempFromJson(Map<String, dynamic> json) {
  return Temp(
      id: json['id'] as int,
      value: (json['value'] as num).toDouble(),
      hour: json['hour'] as int,
      timeCaptured: json['timeCaptured'] as String,
      maxTemp: (json['maxTemp'] as num).toDouble(),
      temp: (json['temp'] as num).toDouble(),
      humidity: (json['humidity'] as num).toDouble());
}

Map<String, dynamic> _$TempToJson(Temp instance) => <String, dynamic>{
      'id': instance.id,
      'value': instance.value,
      'hour': instance.hour,
      'timeCaptured': instance.timeCaptured,
      'maxTemp': instance.maxTemp,
      'temp': instance.temp,
      'humidity': instance.humidity
    };
