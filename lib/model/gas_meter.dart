import 'package:json_annotation/json_annotation.dart';
part 'gas_meter.g.dart';

@JsonSerializable(nullable: false)
class GasMeter{
  int id;
  double value;
  int hour;
  String timeCaptured;
  double maxTemp;
  double temp;
  double humidity;

  GasMeter(
      {this.id,
        this.value,
        this.hour,
        this.timeCaptured,
        this.maxTemp,
        this.temp,
        this.humidity});


  factory GasMeter.fromJson(Map<String, dynamic> json) => _$GasMeterFromJson(json);
  Map<String, dynamic> toJson() => _$GasMeterToJson(this);
}