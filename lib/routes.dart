import 'package:coolsense_manager_app/views/home/home_page.dart';
import 'package:coolsense_manager_app/views/pages/entity_detail_page/entity_detail_page.dart';
import 'package:coolsense_manager_app/views/pages/login/LoginPage.dart';
import 'package:flutter/material.dart';
import 'package:coolsense_manager_app/views/pages/cover/cover_page.dart';

final routes = {
  '/page/cover_page': (BuildContext context) => new CoverPage(),
  '/page/home': (BuildContext context) => new HomePage(),
  '/page/login': (BuildContext context)=>new LoginPage(),
  '/page/rooms': (BuildContext context) => EntityDetailPage()
};
