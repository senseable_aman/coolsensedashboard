import 'dart:async';
import 'dart:convert';
import 'dart:core';

import 'package:coolsense_manager_app/model/alerts/alerts_data.dart';
import 'package:coolsense_manager_app/model/device_data.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/entity_detail_model.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/gps_device_data.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/temp_humid_model.dart';
import 'package:coolsense_manager_app/model/login/fcm_token_response/fcm_token_response.dart';
import 'package:coolsense_manager_app/model/login/login_response.dart';
import 'package:coolsense_manager_app/model/report_response/report_res_data.dart';
import 'package:coolsense_manager_app/utils/network_utils.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
//  static final baseUrl = "http://192.168.0.71:8181/ColdStorageApp/";
  static final baseUrl = "https://coolsense.senseable.co.in/ColdStorageApplication/";
//  static final baseUrl = "http://testserv.senseable.co.in/ColdStorageApplication/";
//  static final baseUrl = "http://192.168.0.71:9001/ColdStorageApplication/";

  Future<LoginResponse> login(String username, String password) {
    return _netUtil.post(baseUrl+"login", headers: requestHeaders,
        body: json.encode({
          'username': username,
          'password': password
        })).then((dynamic res) {
      print(res.toString());
      // if(null) throw new Exception("something went wrong");
      return new LoginResponse.fromJson(res);
    });
  }

  Map<String, String> requestHeaders = {
    'Content-Type':'application/json',
    'referer':'https://coolsense.co.in/'
  };


  Future<FcmTokenResponse> saveFcmToken(String token, String userId, String deviceToken) async {
    Map<String, String> deviceRequestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    print("aman "+token);
    return _netUtil.post(baseUrl+"saveFcmToken", headers: deviceRequestHeaders,
        body: json.encode({
          'user_id': userId,
          'device_token': deviceToken,
          'status':'A',
          'created_dt':DateTime.now().toString()
        })).then((dynamic res) {
          return new FcmTokenResponse.fromJson(res);
    });
  }


  Future<List<DeviceData>> getDevices(String token) async {
    Map<String, String> deviceRequestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    print("aman "+token);
    return _netUtil.get(baseUrl+"getAllDevices", headers: deviceRequestHeaders).then((dynamic res) {
      return List<DeviceData>.from(res.map((i) => DeviceData.fromJson(i)));
    });
  }

  Future<EntityDetailData> getEntityDetail(String token, String locationId, String entityId){
    Map<String, String> entityRequestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    return _netUtil.get(baseUrl+"entity/"+locationId+"/"+entityId, headers: entityRequestHeaders).then((dynamic res) {
      return new EntityDetailData.fromJson(res);
    });

  }


  Future<TempHumidGraphData> getTemperatureDetail(String token, String entityId){
    Map<String, String> requestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    return _netUtil.get(baseUrl+"thLineGraphAmcharts/"+entityId+"?duration=D&param=temperature", headers: requestHeaders).then((dynamic res) {
      return new TempHumidGraphData.fromJson(res);
    });

  }

  Future<TempHumidGraphData> getHumidityDetail(String token, String entityId){
    Map<String, String> requestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    return _netUtil.get(baseUrl+"thLineGraphAmcharts/"+entityId+"?duration=D&param=humidity", headers: requestHeaders).then((dynamic res) {
      return new TempHumidGraphData.fromJson(res);
    });

  }

  Future<TempHumidGraphData> getDeviceGraphDetail(String token, String entityId, String interval){
    Map<String, String> requestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    print("token :"+token + " : "+ entityId);
    return _netUtil.get(baseUrl+"getDeviceDataDeviceId?deviceId="+entityId+"&interval="+interval, headers: requestHeaders).then((dynamic res) {
      return new TempHumidGraphData.fromJson(res);
    });
  }



  Future<List<AlertsData>> getAlertsData(String token){
    Map<String, String> requestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    return _netUtil.get(baseUrl+"alertOverview", headers: requestHeaders).then((dynamic res) {
      return List<AlertsData>.from(res.map((i) =>  AlertsData.fromJson(i)));
    });

  }


  Future<String> logout(String username, String password) {
    return _netUtil.postLogout(baseUrl+"logout",headers: requestLogoutHeaders,
        body: json.encode({
          'username': username,
          'password': password
        })).then((dynamic res) {
      print(res.toString());
      // if(null) throw new Exception("something went wrong");
      return res;
    });
  }
  Map<String, String> requestLogoutHeaders = {
    'Content-Type':'application/json'
  };

  Future<ReportResData> getReport(String startDateTime, String endDateTime, String token, String entityId) {
    Map<String, String> requestReportHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };
    print("date : "+startDateTime + " "+endDateTime);
    return _netUtil.get(baseUrl+"ondemandReport/"+entityId+"/?startDate="+startDateTime+"&endDate="+endDateTime,headers: requestReportHeaders,).then((dynamic res) {
      print("res : "+res.toString());
      // if(null) throw new Exception("something went wrong");
      return new ReportResData.fromJson(res);
    });
  }


  Future<List<GpsDeviceData>> getGpsDeviceData(String token, String device_id) async {
    Map<String, String> gpsDeviceRequestHeaders = {
      'Content-Type':'application/json',
      'X-Auth-Token':token
    };

    return _netUtil.get(baseUrl+"gpsData?deviceId="+device_id, headers: gpsDeviceRequestHeaders).then((dynamic res) {
      return List<GpsDeviceData>.from(res.map((i) => GpsDeviceData.fromJson(i)));
    });
  }

//  return _netUtil.get(baseUrl+"ondemandReport/"+entityId+"/?startDate="+startDateTime+"&endDate="+endDateTime,headers: requestReportHeaders,).then((dynamic res) {



  }