import 'package:connectivity/connectivity.dart';
import 'package:coolsense_manager_app/interfaces/rest_ds.dart';
import 'package:coolsense_manager_app/loader/color_loader.dart';
import 'package:coolsense_manager_app/model/login/fcm_token_response/fcm_token_response.dart';
import 'package:coolsense_manager_app/model/login/login_response.dart';
import 'package:coolsense_manager_app/utils/shared_preferences_manager.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginViewModel extends Model {

  FirebaseMessaging _firebaseMessaging;
  FirebaseMessaging get firebaseMessaging => _firebaseMessaging;
  set firebaseMessaging(FirebaseMessaging value) {
    _firebaseMessaging = value;
    notifyListeners();
  }

  LoginResponse _loginResponse;
  LoginResponse get loginResponse => _loginResponse;
  set loginResponse(LoginResponse value) {
    _loginResponse = value;
    notifyListeners();
  }


  FcmTokenResponse _fcmDeviceToken;
  FcmTokenResponse get fcmDeviceToken => _fcmDeviceToken;
  set fcmDeviceToken(FcmTokenResponse value) {
    _fcmDeviceToken = value;
    notifyListeners();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  bool _internetConnection = false;
  bool get internetConnection => _internetConnection;
  set internetConnection(bool value) {
    _internetConnection = value;
    notifyListeners();
  }


  String _username = "";
  String get username => _username;
  set username(String value) {
    _username = value;
    notifyListeners();
  }

  String _password = "";
  String get password => _password;
  set password(String value) {
    _password = value;
    notifyListeners();
  }

  String _fcmToken = "";
  String get fcmToken => _fcmToken;
  set fcmToken(String value) {
    _fcmToken = value;
    notifyListeners();
  }

  void doLogin(BuildContext context,String username, String password) {
    //if(internetConnection) {
      RestDatasource api = new RestDatasource();
      isLoading = true;
      api.login(username, password).then((LoginResponse response) {
        isLoading = false;
        loginResponse = response;
        saveDeviceTokenToServer(loginResponse.token, loginResponse.id.toString(), fcmToken);
        setToken(response.token);
        setIsLogin(true);
        setUserName(response.userName);
        setPassword(password);
        setUserEmail(response.loginId);
        openHome(context);
        print("Res :" + loginResponse.toString());
      }).catchError((Object error) {
        isLoading = false;
        print("Error : "+error.toString());
      });
//    }
//    else {
//      showToast("No Internet connection");
//    }

  }

  void saveDeviceTokenToServer(String token, String userId, String deviceToken) {
    //if(internetConnection) {
    RestDatasource api = new RestDatasource();
    isLoading = true;
    api.saveFcmToken(token, userId, deviceToken).then((FcmTokenResponse response)  {
      isLoading = false;
      fcmDeviceToken = response;
      print("Res :"+fcmDeviceToken.toString());
    }).catchError((Object error) {
      isLoading = false;
      print("Error : "+error.toString());
    });
//    }
//    else {
//      showToast("No Internet connection");
//    }

  }

  void setToken(String token) async{
    await new SharedPreferencesManager().setToken(token);
  }

  void setUserName(String userName) async{
    await new SharedPreferencesManager().setUserName(userName);
  }

  void setUserEmail(String userEmail) async{
    await new SharedPreferencesManager().setUserEmail(userEmail);
  }



  void setPassword(String password) async{
    await new SharedPreferencesManager().setPassword(password);
  }

  void setIsLogin(bool isLogin) async{
    await new SharedPreferencesManager().setIsLogin(isLogin);
  }


  void openHome(BuildContext context) {
    Navigator.of(context).pushReplacementNamed("/page/home");
  }

  void animatedLoader(){
    ColorLoader(
      color1: Colors.redAccent,
      color2: Colors.green,
      color3: Colors.amber
    );
  }


  void checkInternetConnection(ConnectivityResult result) {
    if (result == ConnectivityResult.mobile || result == ConnectivityResult.wifi) {
      internetConnection = true;
    }
    else
      internetConnection = false;
  }

  void showToast(String msg){
     Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 16.0
    );

  }


  void initializeFireBase(){
    _firebaseMessaging = FirebaseMessaging();
  }
  void register(){
    _firebaseMessaging.getToken().then((token) {
    fcmToken = token;
   // print("token : "+ fcmToken+":"+token);
  });

  }
  void getMessage(){
    _firebaseMessaging.configure(
        onMessage: (Map<String, dynamic> message) async {
          print('on message $message');
        },
        onResume: (Map<String, dynamic> message) async {
          print('on resume $message');
        },
        onLaunch: (Map<String, dynamic> message) async {
          print('on launch $message');
        });
  }
}