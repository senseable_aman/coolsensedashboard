
import 'package:coolsense_manager_app/interfaces/rest_ds.dart';
import 'package:coolsense_manager_app/utils/shared_preferences_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:scoped_model/scoped_model.dart';

class ProfileFragmentViewModel extends Model {

  var context;

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  String _userName = "";
  String get userName => _userName;
  set userName(String value) {
    _userName = value;
    notifyListeners();
  }

  String _userEmail = "";
  String get userEmail => _userEmail;
  set userEmail(String value) {
    _userEmail = value;
    notifyListeners();
  }

  String _userPassword = "";
  String get userPassword => _userPassword ;
  set userPassword(String value) {
    _userPassword  = value;
    notifyListeners();
  }

  void buildContext(BuildContext context){
    this.context = context;
  }

  void doLogout(String username, String password) {
    //if(internetConnection) {
    RestDatasource api = new RestDatasource();
    isLoading = true;
    api.logout(username, password).then((String response)  {
      isLoading = false;
      print(response+">>>>");
      backToLogin();
      clearAllPref();
    }).catchError((Object error) {
      print("Errroor : "+ error.toString());
      isLoading = false;
    });
//    }
//    else {
//      showToast("No Internet connection");
//    }
  }

  void getUserName() async{
    String username = await new SharedPreferencesManager().getUserName();
    userName = username;
    print("username : "+userName);
  }

  void getPassword() async{
    String password = await new SharedPreferencesManager().getPassword();
    userPassword = password;
    print("username : "+userPassword);
  }

  void getUserEmail() async{
    String useremail = await new SharedPreferencesManager().getUserEmail();
    userEmail = useremail;
    print("username : "+userEmail);
  }

  void clearAllPref() async{
    await new SharedPreferencesManager().clearAllPref();
  }

  void backToLogin() {
    Navigator.of(context).pushReplacementNamed("/page/login");
  }

}