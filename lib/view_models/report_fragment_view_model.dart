
import 'package:coolsense_manager_app/interfaces/rest_ds.dart';
import 'package:coolsense_manager_app/model/device_data.dart';
import 'package:coolsense_manager_app/model/report_response/report_res_data.dart';
import 'package:coolsense_manager_app/utils/shared_preferences_manager.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:url_launcher/url_launcher.dart';


class ReportFragmentViewModel extends Model {

  final format = DateFormat("yyyy-MM-dd");
  final time_format = DateFormat("HH:mm:ss");

  int _selectedRadio = 0 ;
  int get selectedRadio => _selectedRadio;
  set selectedRadio(int value) {
    _selectedRadio = value;
    notifyListeners();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  bool _isShowCustom = false;
  bool get isShowCustom => _isShowCustom;
  set isShowCustom(bool value) {
    _isShowCustom = value;
    notifyListeners();
  }

  List<DeviceData> _deviceList = [];
  List<DeviceData> get deviceList => _deviceList;
  set deviceList(List<DeviceData> list){
    _deviceList = list;
    notifyListeners();
  }

  String _deviceId ;
  String get deviceId => _deviceId;
  set deviceId(String value) {
    _deviceId = value;
    notifyListeners();
  }

  DeviceData _deviceData;
  DeviceData get deviceData => _deviceData;
  set deviceData(DeviceData deviceData){
    _deviceData = deviceData;
    notifyListeners();
  }

  ReportResData _reportResData;
  ReportResData get reportResData => _reportResData;
  set reportResData(ReportResData reportResData){
    _reportResData= reportResData;
    notifyListeners();
  }




  setSelectedRadio(int val){
    selectedRadio = val;
  }


  setShowCustom(bool val){
    isShowCustom = val;
  }



  String _token = "";
  String get token => _token;
  set token(String value) {
    _token = value;
    notifyListeners();
  }

  String _entityName = "";
  String get entityName => _entityName;
  set entityName(String value) {
    _entityName= value;
    notifyListeners();
  }

  String _startDate = "";
  String get startDate => _startDate;
  set startDate(String value) {
    _startDate= value;
    notifyListeners();
  }

  String _startTime = "";
  String get startTime => _startTime;
  set startTime(String value) {
    _startTime= value;
    notifyListeners();
  }

  String _endDate = "";
  String get endDate => _endDate ;
  set endDate(String value) {
    _endDate = value;
    notifyListeners();
  }

  String _endTime = "";
  String get endTime => _endTime;
  set endTime(String value) {
    _endTime = value;
    notifyListeners();
  }



  String _endDateTime = "";
  String get endDateTime => _endDateTime;
  set endDateTime(String value) {
    _endDateTime = value;
    notifyListeners();
  }




  void getDeviceData() {
    RestDatasource api = new RestDatasource();
    isLoading = true;
    api.getDevices(token).then((List<DeviceData> response) {
      isLoading = false;
      deviceList = response;


    }).catchError((Object error) {
      isLoading = false;
      print("Error : "+error.toString());
    });

  }

  void getReportData(String startDate, String endDate) {
    RestDatasource api = new RestDatasource();
    isLoading = true;
    api.getReport(startDate,endDate, token, deviceId).then((ReportResData response) {

      reportResData = response;
      launchURL(response.data);

    }).catchError((Object error) {
      isLoading = false;
      print("Error : "+error.toString());
    });

  }


  void getToken() async{

    String t = await new SharedPreferencesManager().getToken();
    token = t;

  }


  void setEntityId(String entityId) async{
    await new SharedPreferencesManager().setEntityId(entityId);
  }


  void launchURL(String url) async {
    isLoading = false;
   var fileName = url.split("/");
    print("url : "+fileName[10]);
    if (await canLaunch("http://testserv.senseable.co.in/ColdStorageApplication/jasper/pdf/"+fileName[10])) {
      await launch("http://testserv.senseable.co.in/ColdStorageApplication/jasper/pdf/"+fileName[10]);

      print("Id: " +deviceId.toString());
    } else {
      throw 'Could not launch $url';
    }
  }

  void toastMsg(String msg){
    Fluttertoast.showToast(
        msg: msg,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1
    );
  }

}