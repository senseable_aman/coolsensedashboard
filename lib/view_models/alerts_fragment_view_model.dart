import 'package:coolsense_manager_app/interfaces/rest_ds.dart';
import 'package:coolsense_manager_app/model/alerts/alerts_data.dart';
import 'package:coolsense_manager_app/utils/shared_preferences_manager.dart';
import 'package:scoped_model/scoped_model.dart';

class AlertsFragmentViewModel extends Model {

  List<AlertsData> _alertsList= [];
  List<AlertsData> get alertsList => _alertsList;
  set alertsList(List<AlertsData> list){
    _alertsList = list;
    notifyListeners();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  String _token = "";
  String get token => _token;
  set token(String value) {
    _token = value;
    notifyListeners();
  }

  void getAlertsData(){
    RestDatasource api = new RestDatasource();
    isLoading = true;
    api.getAlertsData(token).then((List<AlertsData> response) {
      isLoading = false;
      alertsList = response;
    }).catchError((Object error) {
      isLoading = false;
      print("Error : "+error.toString());
    });
  }

  void getToken() async {
    String t = await new SharedPreferencesManager().getToken();
    token = t;

  }

}