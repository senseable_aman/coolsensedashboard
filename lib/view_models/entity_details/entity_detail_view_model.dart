
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:coolsense_manager_app/interfaces/rest_ds.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/entity_detail_model.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/gas_meter.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/gps_address_list.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/gps_device_data.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/humidity.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/temp.dart';
import 'package:coolsense_manager_app/model/entity_detail_model/temp_humid_model.dart';
import 'package:coolsense_manager_app/utils/shared_preferences_manager.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:scoped_model/scoped_model.dart';

class EntityDetailViewModel extends Model {
  bool animate;
  var formatter = new DateFormat('yyyy-MM-dd hh:mm:ss');
  var formatter1 = new DateFormat.yMMM();
  var currentLocation;
  GPSAddressList list;


  BitmapDescriptor _pinLocationIcon;

  BitmapDescriptor get pinLocationIcon => _pinLocationIcon;

  set pinLocationIcon(BitmapDescriptor list) {
    _pinLocationIcon = list;
    notifyListeners();
  }

  EntityDetailData _entityDetailDataList;

  EntityDetailData get entityDetailDataList => _entityDetailDataList;

  set entityDetailDataList(EntityDetailData list) {
    _entityDetailDataList = list;
    notifyListeners();
  }


  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  String _token = "";
  String get token => _token;
  set token(String value) {
    _token = value;
    notifyListeners();
  }


  String _locationId = "";
  String get locationId => _locationId;
  set locationId(String value) {
    _locationId = value;
    notifyListeners();
  }


  String _entityId = "";
  String get entityId => _entityId;
  set entityId(String value) {
    _entityId = value;
    notifyListeners();
  }


  String _interval = "24 hour";
  String get interval => _interval;
  set interval(String value) {
    _interval = value;
    notifyListeners();
  }


  TempHumidGraphData _tempHumidGraphData;
  TempHumidGraphData get tempHumidGraphData => _tempHumidGraphData;
  set tempHumidGraphData(TempHumidGraphData list) {
    _tempHumidGraphData = list;
    notifyListeners();
  }

  List<charts.Series<Temp, DateTime>> _createTempGraphData ;
  List<charts.Series<Temp, DateTime>> get createTempGraphData => _createTempGraphData;
  set createTempGraphData(List<charts.Series<Temp, DateTime>> list) {
    _createTempGraphData = list;
    notifyListeners();
  }

  List<charts.Series<Humidity, DateTime>> _createHumidGraphData;
  List<charts.Series<Humidity, DateTime>> get createHumidGraphData => _createHumidGraphData;
  set createHumidGraphData(List<charts.Series<Humidity, DateTime>> list) {
    _createHumidGraphData = list;
    notifyListeners();
  }

  List<charts.Series<GasMeter, DateTime>> _createGasMeterGraphData = [];
  List<charts.Series<GasMeter, DateTime>> get createGasMeterGraphData => _createGasMeterGraphData;
  set createElectricityGraphData(List<charts.Series<GasMeter, DateTime>> list) {
    _createGasMeterGraphData = list;
    notifyListeners();
  }


  List<Temp> _tempList = [];
  List<Temp> get tempList => _tempList;
  set tempList(List<Temp> list) {
    _tempList = list;
    notifyListeners();
  }

  List<Humidity> _humidDataList = [];
  List<Humidity> get humidDataList => _humidDataList;
  set humidDataList(List<Humidity> list) {
    _humidDataList = list;
    notifyListeners();
  }

  List<GasMeter> _gasMeterDataList = [];
  List<GasMeter> get gasMeterDataList => _gasMeterDataList;
  set gasMeterDataList(List<GasMeter> list) {
    _gasMeterDataList = list;
    notifyListeners();
  }

  List<GasMeter> _gasMeterDataList1 = [];
  List<GasMeter> get gasMeterDataList1 => _gasMeterDataList1;
  set gasMeterDataList1(List<GasMeter> list) {
    _gasMeterDataList1 = list;
    notifyListeners();
  }

  List<GpsDeviceData> _gpsDataList = [];
  List<GpsDeviceData> get gpsDataList => _gpsDataList;
  set gpsDataList(List<GpsDeviceData> list) {
    _gpsDataList = list;
    notifyListeners();
  }

  List<Marker> _markers = [];
  List<Marker> get markers => _markers;
  set markers(List<Marker> list) {
    _markers = list;
    notifyListeners();
  }

  List<GPSAddressList> _address = [];
  List<GPSAddressList> get address => _address;
  set address(List<GPSAddressList> value) {
    _address = value;
    notifyListeners();
  }


  List<bool> _isSelected = [true, false];
  List<bool> get isSelected => _isSelected;
  set isSelected(List<bool> value) {
    _isSelected = value;
    notifyListeners();
  }

  addDeviceGraphData() {
    RestDatasource api = new RestDatasource();
    isLoading = true;
    print("interval "+ interval);
    api.getDeviceGraphDetail(token, entityId, interval).then((
        TempHumidGraphData response) {
      isLoading = false;
     // print("aaa :" + response.toString());
      getGraphData(response);
    }).catchError((Object error) {
      isLoading = false;
      print("Error : " + error.toString());
    });
  }

  getGraphData(TempHumidGraphData res) {
    if (res.temp.length > 0 && res.gasMeter.length == 0 &&
        res.humidity.length > 0 && res.type=='TH') {
      tempHumidGraphData = res;
      tempList = res.temp;
      print("Size : "+tempList.length.toString());
      var result = tempList.map((m) => m.value).reduce((a, b) => a + b) / tempList.length;

      humidDataList = res.humidity;
      var result1 = humidDataList.map((m) => m.value).reduce((a, b) => a + b) / humidDataList.length;
      createTempGraphData = [
        new charts.Series(
          id: 'average',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (Temp sales, _) => DateTime.parse(sales.time_captured),
          measureFn: (Temp sales, _) => result,
          data: tempList,
        )// Configure our custom bar target renderer for this series.
        ..setAttribute(charts.rendererIdKey, 'customArea'),
        new charts.Series(
          id: 'temp',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (Temp graphPoints, _) => DateTime.parse(graphPoints.time_captured),
          measureFn: (Temp graphPoints, _) => graphPoints.value,
          data: tempList,
        ),

      ];

      createHumidGraphData = [
        new charts.Series(
          id: 'average',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (Humidity sales, _) => DateTime.parse(sales.time_captured),
          measureFn: (Humidity sales, _) => result1,
          data: humidDataList,
        )// Configure our custom bar target renderer for this series.
        ..setAttribute(charts.rendererIdKey, 'customArea'),
        new charts.Series(
          id: 'humidity',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (Humidity graphPoints, _) =>DateTime.parse(graphPoints.time_captured),
          measureFn: (Humidity graphPoints, _) => graphPoints.value,
          data: humidDataList,
        )
      ];
    }
    else if (res.temp.length > 0 && res.gasMeter.length == 0 && res.type=='T' ) {

      tempHumidGraphData = res;
      tempList = res.temp;
      var result = tempList.map((m) => m.value).reduce((a, b) => a + b) / tempList.length;

      createTempGraphData = [
        new charts.Series(
          id: 'average',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (Temp sales, _) => DateTime.parse(sales.time_captured),
          measureFn: (Temp sales, _) => result,
          data: tempList,
        )
        // Configure our custom bar target renderer for this series.
          ..setAttribute(charts.rendererIdKey, 'customArea'),
        new charts.Series(
          id: 'temp',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (Temp graphPoints, _) => DateTime.parse(graphPoints.time_captured),
          measureFn: (Temp graphPoints, _) => graphPoints.value,
          data: tempList,
        ),
//        )..setAttribute(charts.rendererIdKey, 'customLine')..setAttribute(charts.boundsLineRadiusPxKey, result),

      ];
    }
    else if (res.temp.length > 0 && res.gasMeter.length == 0 && res.type=='TEXT' ) {
      tempHumidGraphData = res;
      tempList = res.temp;
      var result = tempList.map((m) => m.value).reduce((a, b) => a + b) / tempList.length;

      createTempGraphData = [
        new charts.Series(
          id: 'average',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (Temp sales, _) => DateTime.parse(sales.time_captured),
          measureFn: (Temp sales, _) => result,
          data: tempList,
        )
        // Configure our custom bar target renderer for this series.
          ..setAttribute(charts.rendererIdKey, 'customArea'),
        new charts.Series(
          id: 'temp',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (Temp graphPoints, _) => DateTime.parse(graphPoints.time_captured),
          measureFn: (Temp graphPoints, _) => graphPoints.value,
          data: tempList,
        )
      ];
    }
    else if (res.temp.length == 0 && res.gasMeter.length > 0 &&
        res.humidity.length == 0) {
      gasMeterDataList = res.gasMeter;
      for (int i = 0; i <= gasMeterDataList.length; i++) {
        if (i <= gasMeterDataList.length - 2) {
          double value = gasMeterDataList[i+1].value -
              gasMeterDataList[i].value;
          GasMeter gasmeter1 = new GasMeter();
          print("value" + value.toString());
          gasmeter1.value = value/10;
          gasmeter1.time_captured = gasMeterDataList[i].time_captured;
          gasMeterDataList1.add(gasmeter1);
          print("size : " + gasMeterDataList1.length.toString());
        }
      }

      // print("size : "+ gasMeterDataList1.length.toString());
      //  tempHumidGraphData = res;
      createElectricityGraphData = [
        new charts.Series(
          id: 'value',
          colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
          domainFn: (GasMeter graphPoints, _) =>
              DateTime.parse(
                  formatter.format(DateTime.parse(graphPoints.time_captured))),
          measureFn: (GasMeter graphPoints, _) => graphPoints.value,
          data: gasMeterDataList1,
        )
      ];
    }
  }

  getGpsDeviceData() {
    RestDatasource api = new RestDatasource();
    isLoading = true;
    api.getGpsDeviceData(token, entityId).then((List<GpsDeviceData> response) {
      isLoading = false;
      gpsDataList = response;

      for (int i = 0; i < gpsDataList.length; i++) {
        addMarker(i);
        getAddress(gpsDataList[i].latitude, gpsDataList[i].longitude,
            gpsDataList[i].time_captured1);

      }
      /* Marker marker = Marker(
        markerId: MarkerId(gpsDataList.length.toString()),
        position: LatLng(currentLocation.latitude, currentLocation.longitude),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueGreen),
        infoWindow: const InfoWindow(
          title: 'My Marker',
        ),
      );*/
      //markers.add(marker);
    }).catchError((Object error) {
      isLoading = false;
      print("Error : " + error.toString());
    });
  }


  void addMarker(int i) {
    Marker marker = Marker(
      markerId: MarkerId(i.toString()),
      position: LatLng(gpsDataList[i].latitude, gpsDataList[i].longitude),
      icon: pinLocationIcon,
      infoWindow: const InfoWindow(
        title: 'My Marker',
      ),
    );
    markers.add(marker);
  }


  void getToken() async {
    String t = await new SharedPreferencesManager().getToken();
    token = t;
  }

  void getLocationId() async {
    String location = await new SharedPreferencesManager().getLocationId();
    locationId = location;
    print("id: " + locationId);
  }

  void getEntityId() async {
    String entity = await new SharedPreferencesManager().getEntityId();
    entityId = entity;
    print("entity id: " + entityId);
  }

  String totalConsumed() {
    double total = 0.0;

    gasMeterDataList1.forEach((GasMeter gasMeter) {
      total = total + gasMeter.value;
    });

    return total.toStringAsFixed(1);
  }


  void setMarkers(List<GpsDeviceData> gpsDataList) {
    markers = gpsDataList.map((n) {
      LatLng point = LatLng(n.latitude, n.longitude);
    }).toList();
  }

  void getLocation() async {
    currentLocation = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
  }


  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        'images/marker_icon.png');
  }

  getAddress(double latitude, double longitude, String timeCaptured) async {
    final coordinates = new Coordinates(latitude, longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;
    list = new GPSAddressList();
    print("adreesss :" + "${first.featureName} : ${first.addressLine}");
    list.timeCaptured = timeCaptured;
    list.addresses = first.addressLine.toString();
    list.latitude = latitude;
    list.longitude = longitude;
    address.add(list);
    print("Size : " + address.length.toString());
    return first.addressLine.toString();
  }

}