import 'dart:async';
import 'package:coolsense_manager_app/utils/shared_preferences_manager.dart';

import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';
class CoverViewModel extends Model {
  var context;
  bool _isLogin;
  bool get isLogin => _isLogin;
  set isLogin(bool value) {
    _isLogin = value;
    notifyListeners();
  }

  void buildContext(BuildContext context){
    this.context = context;
  }
  void openLogin() {
    Navigator.of(context).pushReplacementNamed("/page/login");
  }
  void openHome() {
    Navigator.of(context).pushReplacementNamed("/page/home");
  }
  startTimer() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, ( ) => isLogin ? openHome() : openLogin());
  }
  void getIsLogin() async {
    bool t = await new SharedPreferencesManager().getIsLogin();
    isLogin = t;
    print("isLogin : "+isLogin.toString());
  }

}