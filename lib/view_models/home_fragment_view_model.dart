
import 'package:coolsense_manager_app/interfaces/rest_ds.dart';
import 'package:coolsense_manager_app/model/device_data.dart';
import 'package:coolsense_manager_app/utils/shared_preferences_manager.dart';
import 'package:coolsense_manager_app/views/pages/entity_detail_page/entity_detail_page.dart';
import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:scoped_model/scoped_model.dart';

class HomeFragmentViewModel extends Model {
  var context;

  List<DeviceData> _deviceList = [];
  List<DeviceData> get deviceList => _deviceList;
  set deviceList(List<DeviceData> list){
    _deviceList = list;
    notifyListeners();
  }

  List<DeviceData> _filterDeviceList = [];
  List<DeviceData> get filterDeviceList => _filterDeviceList;
  set filterDeviceList(List<DeviceData> list){
    _filterDeviceList = list;
    notifyListeners();
  }

  bool _isLoading = false;
  bool get isLoading => _isLoading;
  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  String _token = "";
  String get token => _token;
  set token(String value) {
    _token = value;
    notifyListeners();
  }

  String _address = "";
  String get address => _address;
  set address(String value) {
    _address = value;
    notifyListeners();
  }


  void buildContext(BuildContext context){
    this.context = context;
  }

  void getDeviceData() {
    RestDatasource api = new RestDatasource();
    isLoading = true;
    api.getDevices(token).then((List<DeviceData> response) {
      isLoading = false;
      deviceList = response;
      filterDeviceList = response;
       getLocation(filterDeviceList[0].gpsCompVo.latitude,
          filterDeviceList[0].gpsCompVo.longitude);

    //  print("RES " + response.toString());
    }).catchError((Object error) {
      isLoading = false;

      if(error.toString().split(":")[1].trim()==401){
        openLogin();
      }
     // print("Error : "+error.toString().split(":")[1]);

    });
  }

  void openLogin() {
    Navigator.of(context).pushReplacementNamed("/page/login");
  }

  void clickRoomWiseData(BuildContext context, int index){
    setEntityId(filterDeviceList[index].deviceId.toString());
    Navigator.push(context, MaterialPageRoute(builder: (context)=> EntityDetailPage(name:filterDeviceList[index].entityName)));
  }

  void getToken() async{
    String t = await new SharedPreferencesManager().getToken();
    token = t;
  }

  void filterSearchResults(String query) {
    filterDeviceList = deviceList.where((device)=> device.deviceName.toLowerCase().contains(query.toLowerCase())).toList();
  }

  void setLocationId(String locationId) async{
    await new SharedPreferencesManager().setLocationId(locationId);
  }

  void setEntityId(String entityId) async{
    await new SharedPreferencesManager().setEntityId(entityId);
  }

    getLocation(double latitude, double longitude) async {
    final coordinates = new Coordinates(latitude, longitude);
    var addresses = await Geocoder.local.findAddressesFromCoordinates(
        coordinates);
    var first = addresses.first;

    print("adreesss :"+"${first.featureName} : ${first.addressLine}");
    address = first.addressLine.toString();
    return first.addressLine.toString();
  }

}