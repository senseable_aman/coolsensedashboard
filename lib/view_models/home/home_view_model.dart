import 'package:coolsense_manager_app/views/fragments/alerts_fragment.dart';
import 'package:coolsense_manager_app/views/fragments/home_fragment.dart';
import 'package:coolsense_manager_app/views/fragments/profile_fragment.dart';
import 'package:coolsense_manager_app/views/fragments/reports_fragment.dart';
import 'package:scoped_model/scoped_model.dart';

class HomeViewModel extends Model {

  int _bottomNavigationBarIndex = 0;

  int get bottomNavigationBarIndex => _bottomNavigationBarIndex;

  set bottomNavigationBarIndex(int value) {
    _bottomNavigationBarIndex = value;
    notifyListeners();
  }

  var fragments = [
    new HomeFragment(),
    new AlertsFragment(),
    new ReportFragment(),
    new ProfileFragment()
  ];
}
